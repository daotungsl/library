﻿using System;
using System.Configuration;

namespace TN.Service
{
    public class AppSettings
    {
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];
        public static string SocketServerIP = ConfigurationManager.AppSettings["SocketServerIP"];
        public static int SocketServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["SocketServerPort"]);
        public static int WriteConsoleOrLog = Convert.ToInt32(ConfigurationManager.AppSettings["WriteConsoleOrLog"]);
        public static int SocketConnectionTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SocketConnectionTimeout"]);
        public static int CheckConnectionInterval = Convert.ToInt32(ConfigurationManager.AppSettings["CheckConnectionInterval"]);
        public static string ServerFolder = ConfigurationManager.AppSettings["ServerFolder"];
        public static string BackendDataFolder = ConfigurationManager.AppSettings["BackendDataFolder"];
        public static int DataSyncIntervalSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["DataSyncIntervalSeconds"]);
        //BackendFolderData

        public static int CheckLiveIntervalSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["CheckLiveIntervalSeconds"]);
    }
}