﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TN.Service
{
    public static partial class Functions
    {
        public static string HMACMD5Password(string matkhau)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(matkhau));
                var strResult = BitConverter.ToString(result);
                return strResult.Replace("-", "").Substring(5);
            }
        }
        public static string GenFolderName()
        {
            return DateTime.Now.ToString("yyyMMddHHmmss");
        }
        public static bool BackupFile(string UrlData,string urlFileBackUp)
        {
            if (!Directory.Exists(UrlData))
            {
                Directory.CreateDirectory(UrlData);
            }
            if (!Directory.Exists(urlFileBackUp))
            {
                Directory.CreateDirectory(urlFileBackUp);
            }
            else
            {
                return true;
            }
            string[] files = Directory.GetFiles(UrlData, "*.zip");
            for (int j = 0; j <= files.Length - 1; j++)
            {
                string urlFile = files[j];
                if (!Utilities.IsFileLocked(new FileInfo(urlFile)))
                {
                    File.Copy(urlFile, urlFileBackUp + Path.GetFileName(urlFile));
                }
            }
            return true;
        }
        public static bool DeleteFileInFolder(string folderPath)
        {
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                    return true;
                }
                
                //xóa file thư mục gốc
                string[] filesGoc = Directory.GetFiles(folderPath);
                for (int j = 0; j <= filesGoc.Length - 1; j++)
                {
                    string urlFile = filesGoc[j];
                    if (!Utilities.IsFileLocked(new FileInfo(urlFile)))
                    {
                        File.Delete(urlFile);
                    }
                }
                // Xóa file các thư mục con
                var listFolder = Directory.GetDirectories(folderPath);
                foreach (var item in listFolder)
                {
                    string[] files = Directory.GetFiles(item);
                    for (int j = 0; j <= files.Length - 1; j++)
                    {
                        string urlFile = files[j];
                        if (!Utilities.IsFileLocked(new FileInfo(urlFile)))
                        {
                            File.Delete(urlFile);
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
            
        }
       
        public  static object GetPropertyInObject(object input,string name)
        {
           return input.GetType().GetProperty(name).GetValue(input, null);
        }
        public static bool IsNumber(this object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }
    }
}
