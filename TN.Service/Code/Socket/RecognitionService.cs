﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using TN.Service;
using TN.Shared.Service;

namespace TN.Socket
{
    public class RecognitionService
    {
        private static SocketServer _serverLibrary;
        string _healthCheckMsg = "msg.healthcheck()";
        private static modbus _modbus = new modbus();
        public bool StartServerLibrary()
        {
            try
            {
                byte[] healthCheckBytes = Encoding.UTF8.GetBytes(_healthCheckMsg);
                _serverLibrary = new SocketServer(AppSettings.SocketServerIP, AppSettings.SocketServerPort, true, 5 * 1000, 5 * 1000, healthCheckBytes);
                _serverLibrary.ClientConnected += new SocketServer.ClientConnectedHandler(SocketListener_ClientConnected);
                _serverLibrary.DataReceived += new SocketServer.DataReceivedHandler(SocketListener_DataReceived);
                _serverLibrary.ClientDisconnected += new SocketServer.ClientDisconnectedHandler(SocketListener_ClientDisconnected);
                _serverLibrary.Start();
                return true;

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("RecognitionService_Start", ex.ToString());
                return false;

            }
        }

        private void SocketListener_ClientDisconnected(SocketServer listener, string clientip)
        {

        }

        private void SocketListener_DataReceived(SocketServer listener, string clientIp, byte[] data, int byteCount)
        {
            try
            {
                string originalMsg = Encoding.UTF8.GetString(data, 0, byteCount);
                string[] messages = originalMsg.Trim().Split(')');

                foreach (string message in messages)
                {
                    if (string.IsNullOrEmpty(message))
                        continue;

                    string msg = message + ")";
                    var msgarr = msg.TrimEnd(')').Split(';');
                    Debug.WriteLine(msg);
                    if (!message.Contains("healthcheck"))
                    {
                        if (message.Contains("comcheck"))
                        {
                            GetAllComPort(clientIp);
                        }
                        if (AppSettings.WriteConsoleOrLog == 0)
                        {
                            Console.WriteLine($"Server_Received_{clientIp} Msg: {msg.TrimEnd(')')}");

                        }

                        EntityBox entityBox = new EntityBox
                        {
                            COM = Convert.ToInt32(msgarr[0]),
                            BOXID = (msgarr[1]),
                            TRAYID = Convert.ToInt32(msgarr[2])
                        };
                        GetInfoOpenBox(entityBox, clientIp);

                    }
                    else
                    {
                        Debug.WriteLine($"Socket server Received: {msg}");
                    }
                    if (msg == _healthCheckMsg)
                        continue;

                    Utilities.WriteOperationLog("SocketListener_DataReceived", $"Received from {clientIp}: {msg}");

                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SocketListener_DataReceived", ex.ToString());
            }
        }
        private void GetAllComPort(string clientIp)
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                string allPorts = "COM_Empty";
                if (ports.Length != 0)
                {
                    allPorts = String.Join("|", ports);
                }
                _serverLibrary.SendToClient(clientIp, Encoding.UTF8.GetBytes(allPorts));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private void SocketListener_ClientConnected(SocketServer listener, string clientip, bool status)
        {

        }
        public bool StopLibrary()
        {
            try
            {

                _serverLibrary?.Stop();

                return true;
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("RecognitionService_StopClient", ex.ToString());
                return false;
            }
        }

        private static void GetInfoOpenBox(EntityBox entityBox, string clientIp)
        {
            //_modbus = new modbus();
            string msgReceived = null;

            if (AppSettings.WriteConsoleOrLog == 0)
            {
                Console.WriteLine($"Modbus_Open: {_modbus.sp.IsOpen}");
            }
            else if (AppSettings.WriteConsoleOrLog == 1)
            {
                Utilities.WriteDebugLog("Modbus_Open", $"{_modbus.sp.IsOpen}");
            }
            else
            {
                Console.WriteLine($"Modbus_Open: {_modbus.sp.IsOpen}");
                Utilities.WriteDebugLog("Modbus_Open", $"{_modbus.sp.IsOpen}");
            }
            #region [OpenModbus]
            if (!_modbus.sp.IsOpen)
            {
                bool isOpen = _modbus.Open($"COM{entityBox.COM}", 9600, 8, System.IO.Ports.Parity.Even, System.IO.Ports.StopBits.One);

                if (!isOpen)
                {
                    msgReceived = "DK_com_false";
                    _serverLibrary.SendToClient(clientIp, Encoding.UTF8.GetBytes(msgReceived));

                    if (AppSettings.WriteConsoleOrLog == 0)
                    {
                        Console.WriteLine($"Server_Send_{msgReceived}");
                    }
                    else if (AppSettings.WriteConsoleOrLog == 1)
                    {
                        Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                    }
                    else
                    {
                        Console.WriteLine($"Server_Send_{msgReceived}");
                        Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                    }

                }

            }
            if (_modbus.sp.IsOpen)
            {
                if (AppSettings.WriteConsoleOrLog == 0)
                {
                    Console.WriteLine($"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                }
                else if (AppSettings.WriteConsoleOrLog == 1)
                {
                    Utilities.WriteDebugLog("Check_Com: ", $"{_modbus.sp.PortName} == COM{ entityBox.COM}");

                }
                else
                {
                    Console.WriteLine($"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                    Utilities.WriteDebugLog("Check_Com: ", $"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                }

                try
                {
                    if (_modbus.sp.PortName == $"COM{entityBox.COM}")
                    {
                        bool openSuccess = _modbus.Openbox(Convert.ToByte(entityBox.BOXID), Convert.ToByte(entityBox.TRAYID));
                        if (openSuccess)
                        {

                            msgReceived = "DK_opentray_true";
                            _serverLibrary.SendToClient(clientIp, Encoding.UTF8.GetBytes(msgReceived));

                            if (AppSettings.WriteConsoleOrLog == 0)
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                            }
                            else if (AppSettings.WriteConsoleOrLog == 1)
                            {
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                            }
                            else
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                            }

                            _modbus.Close();

                        }
                        else
                        {

                            msgReceived = "DK_opentray_false";
                            _serverLibrary.SendToClient(clientIp, Encoding.UTF8.GetBytes(msgReceived));

                            if (AppSettings.WriteConsoleOrLog == 0)
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                            }
                            else if (AppSettings.WriteConsoleOrLog == 1)
                            {
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                            }
                            else
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                            }

                            _modbus.Close();

                        }
                    }
                    else
                    {
                        msgReceived = "DK_openbox_false";
                        _serverLibrary.SendToClient(clientIp, Encoding.UTF8.GetBytes(msgReceived));

                        if (AppSettings.WriteConsoleOrLog == 0)
                        {
                            Console.WriteLine($"Server_Send_{msgReceived}");
                        }
                        else if (AppSettings.WriteConsoleOrLog == 1)
                        {
                            Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                        }
                        else
                        {
                            Console.WriteLine($"Server_Send_{msgReceived}");
                            Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                        }

                        _modbus.Close();
                    }
                }
                catch (Exception e)
                {
                    if (AppSettings.WriteConsoleOrLog == 0)
                    {
                        Console.WriteLine($"GetInfoOpenBox: {e}");
                    }
                    else if (AppSettings.WriteConsoleOrLog == 1)
                    {
                        Utilities.WriteErrorLog("GetInfoOpenBox", e.ToString());

                    }
                    else
                    {
                        Console.WriteLine($"GetInfoOpenBox: {msgReceived}");
                        Utilities.WriteErrorLog("GetInfoOpenBox", e.ToString());
                    }

                }
            }
            #endregion
        }

    }
}