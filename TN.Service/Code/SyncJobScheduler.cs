﻿using System;
using System.Diagnostics;
using System.Text;
using Quartz;
using Quartz.Impl;
using TN.Service;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;

namespace TN.Service
{
    public class SyncJobScheduler
    {
        private static IScheduler scheduler;

        public static  void Start()
        {
            try
            {
                Utilities.WriteOperationLog("SyncJobScheduler_Start", "Start");
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                // Run data sync job
                if (AppSettings.CheckLiveIntervalSeconds > 0)
                {
                    IJobDetail prepaidSyncJob = JobBuilder.Create<DataSyncJob>().Build();
                    ITrigger prepaidSyncTrigger = TriggerBuilder.Create()
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(AppSettings.CheckLiveIntervalSeconds)
                                .RepeatForever())
                            .Build();
                    scheduler.ScheduleJob(prepaidSyncJob, prepaidSyncTrigger);
                    Utilities.WriteOperationLog("SyncJobScheduler_Start", $"{DateTime.Now} - Start data job, interval {AppSettings.CheckLiveIntervalSeconds} seconds!");
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SyncJobScheduler_Start", ex.ToString());
            }
        }

        public static void Stop()
        {
            try
            {
                if (scheduler != null)
                {
                    scheduler.Shutdown();
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SyncJobScheduler_Stop", ex.ToString());
            }
        }
    }
    public class DataSyncJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("DataSyncJob_Execute", ex.ToString());
            }
        }
    }
   
}
