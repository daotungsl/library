﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Service
{
    public class DeviceStatusModel
    {
        public int DeviceId { get; set; }
        public bool ConnectStatus { get; set; }
        public DateTime? LastLive { get; set; }
    }
    public class ClientResponsiveDataModel
    {
        public string IMEI { get; set; }
        public string Type { get; set; }
        public bool Status { get; set; }
        public string Data { get; set; }
    }
    public class ClientSendDataSocketModel
    {
        public ClientSendDataSocketType Type { get; set; }
        public int DeviceId { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public string IMEI { get; set; }
        public string Data { get; set; }
    }
    public enum ClientSendDataSocketType
    {
        ONOFF,
        CONFIG,
        SYSTEM,
        SYNC
    }
    public class ControlsDevicelModel
    {
        public ControlsDevicelType Type { get; set; }
        public string IPAddress { get; set; }
        public int PORT { get; set; }
        public string Data { get; set; }
        public string IMEI { get; set; }
    }
    public enum ControlsDevicelType
    {
        ONOFF,
        CONFIG,
        SYSTEM,
        SYNC,
        CONVERT,
        SENDDATA,
        FTPSYSTEM,
        DOWNLOAD
    }
    public class DeviceConfigModel
    {
        public int StatusInterval { get; set; }
        public List<DeviceBrightnessModel> Brightness { get; set; }
        public List<DeviceContrastModel> Contrast { get; set; }
        public List<DeviceTimeOffModel> TimeOff { get; set; }
        public List<DeviceTimeOnModel> TimeOn { get; set; }

    }
    public class DeviceBrightnessModel
    {
        public string Time { get; set; }
        public int Value { get; set; }
    }
    public class DeviceContrastModel
    {
        public string Time { get; set; }
        public int Value { get; set; }
    }
    public class DeviceTimeOnModel
    {
        public string Time { get; set; }
    }
    public class DeviceTimeOffModel
    {
        public string Time { get; set; }
    }
    
    public class DeviceSlideModel
    {
        public List<DeviceBlockModel> Blocks { get; set; }
    }
    public class DeviceBlockModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<DeviceBlockContentModel> Contents { get; set; }
    }
    public class DeviceBlockContentModel
    {
        public string Name { get; set; }
        public List<DeviceFrameModel> Frames { get; set; }
    }
    public class DeviceFrameModel
    {
        public DeviceFrameSizeModel Size { get; set; }
        public int Animation { get; set; }
        public List<DeviceFrameFileModel> Files { get; set; }
    }
    public class DeviceFrameSizeModel
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
    public class DeviceFrameFileModel
    {
        public string Path { get; set; }
        public int Duration { get; set; }
        public int? Type { get; set; }
    }
    public enum BannerType
    {
        [Display(Name = "Không xác định")]
        Default = -1,
        [Display(Name = "Hình ảnh")]
        Image =0,
        [Display(Name = "Video")]
        Video = 1,
        [Display(Name = "Văn bản")]
        RickText = 2,
        [Display(Name = "Tài liệu")]
        Clock = 3,
        [Display(Name = "Đồng hồ")]
        Countdown = 4,
        [Display(Name = "Đếm ngược")]
        Document = 5
    }
    public class EntityBox
    {
        public int COM { get; set; }
        public string BOXID { get; set; }
        public int TRAYID { get; set; }
    }
}
