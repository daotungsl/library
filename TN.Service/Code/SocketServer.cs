﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TN.Shared.Service;

namespace TN.Service
{
    public class SocketServer
    {
        private enum CurrentState
        {
            err = -1,
            stopped = 0,
            running = 1,
            idle = 2
        }

        public delegate void ClientConnectedHandler(SocketServer listener, string clientIp, int port, bool status);
        public ClientConnectedHandler ClientConnected;
        public delegate void DataReceivedHandler(SocketServer listener, string clientIp, int port, byte[] data, int byteCount);
        public DataReceivedHandler DataReceived;
        public delegate void ClientDisconnectedHandler(SocketServer listener, string clientIp, int port);
        public ClientDisconnectedHandler ClientDisconnected;

        private TcpListener Listener;
        private int Port;
        private IPEndPoint localEndpoint;
        private Int32 newSessionId = 0;
        public bool IsRunning = false;
        private CurrentState serverState = CurrentState.stopped;
        private Sessions SessionCollection = new Sessions();
        private object _sessionCollectionLocker = new object();
        private int _connectionTimeout = 30000;

        private int _maxConnection = 5000;
        private bool _restarting = false;

        private ConcurrentDictionary<string, SocketClientInfo> connectingClients;
        Timer _healthCheckTimer;

        public SocketServer(string localIp, int prt, int connectionTimeout, int maxConnection = 5000)
        {
            Port = prt;
            if (string.IsNullOrEmpty(localIp))
                localEndpoint = new IPEndPoint(IPAddress.Any, Port);
            else
                localEndpoint = new IPEndPoint(IPAddress.Parse(localIp), Port);

            _connectionTimeout = connectionTimeout;
            _maxConnection = maxConnection;
            CheckCom();
        }

        private void CheckCom()
        {
            string[] ports = SerialPort.GetPortNames();

            foreach (string port in ports)
            {
                Console.WriteLine($"{ port} is active");
                Utilities.WriteDebugLog($"{port} is active");
            }

        }

        public bool Start()
        {
            connectingClients = new ConcurrentDictionary<string, SocketClientInfo>();

            if (serverState == CurrentState.running)
            {
                return false;
            }

            serverState = CurrentState.idle;

            Thread listenerThread = new Thread(TheListener);

            try
            {
                IsRunning = true;

                listenerThread.Name = "Server Listener Thread";
                listenerThread.Start();

                StartTimer();
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
                return false;
            }

            while (serverState != CurrentState.running)
            {
                Thread.Sleep(10);
                if (serverState == CurrentState.err | serverState == CurrentState.stopped)
                {
                    return false;
                }
            }

            return true;
        }

        public void Stop()
        {
            IsRunning = false;
            serverState = CurrentState.stopped;

            try
            {
                StopTimer();
                foreach (string key in connectingClients.Keys)
                {
                    try
                    {
                        connectingClients[key]?.Close();
                    }
                    catch
                    {
                        // ignored
                    }
                }

                Listener.Stop();
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }

            try
            {
                SessionCollection.ShutDown();
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }
        }

        public void Restart()
        {
            if (_restarting)
                return;

            try
            {
                bool success = false;
                do
                {
                    try
                    {
                        _restarting = true;
                        Stop();
                        Thread.Sleep(10000);
                        Start();
                        success = true;
                        _restarting = false;
                    }
                    catch (Exception ex)
                    {
                        Utilities.WriteDebugLog(ex.ToString());
                        Thread.Sleep(3000);
                    }
                } while (!success);
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }
        }

        public void UpdateMaxConnection(int maxConnection)
        {
            _maxConnection = maxConnection;
        }

        private void StartTimer()
        {
            try
            {
                //if (_identifyTimeout > 0)
                //    _verifyClientIdentityTimer = new Timer(VerifyClientIdentity, null, _identifyTimeout / 2, _identifyTimeout / 2);
                _healthCheckTimer = new Timer(CheckClientConnection, null, AppSettings.CheckConnectionInterval, AppSettings.CheckConnectionInterval);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SocketServer_StartTimer", ex.ToString());
            }
        }

        private void StopTimer()
        {
            try
            {
                if (_healthCheckTimer != null)
                {
                    _healthCheckTimer.Dispose();
                    _healthCheckTimer = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SocketServer_StopTimer", ex.ToString());
            }
        }

        private void TheListener()
        {
            try
            {
                bool success = false;
                do
                {
                    try
                    {
                        // Start listening
                        Listener = new TcpListener(localEndpoint);

                        Listener.Start();
                        Utilities.WriteDebugLog($"Start listening on port {Port} on thread {Thread.CurrentThread.ManagedThreadId}");
                        Console.WriteLine($"SocketServer: Start listening on port {Port} on thread {Thread.CurrentThread.ManagedThreadId}");
                        if (StartAccept())
                        {
                            serverState = CurrentState.running;
                            success = true;
                        }
                        else
                        {
                            serverState = CurrentState.err;
                        }
                    }
                    catch (Exception ex)
                    {
                        serverState = CurrentState.err;
                        Utilities.WriteDebugLog(ex.ToString());
                        Thread.Sleep(3000);
                    }
                } while (!success);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("SocketServer_theListener", ex.ToString());
            }
        }

        private bool StartAccept()
        {
            try
            {
                if (IsRunning)
                {
                    Listener.BeginAcceptTcpClient(HandleAsyncConnection, Listener);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }

            return false;
        }

        private void HandleAsyncConnection(IAsyncResult res)
        {
            try
            {
                TcpClient client = default(TcpClient);

                if (!StartAccept())
                    return;
                client = Listener.EndAcceptTcpClient(res);

                IPEndPoint endpoint = (IPEndPoint)client.Client.RemoteEndPoint;
                string clientIp = endpoint.Address.ToString();
                int port = endpoint.Port;

                // neu ton tai ket noi cu, dong ket noi va xoa khoi danh sach
                if (connectingClients.ContainsKey($"{clientIp}:{port}"))
                {
                    try
                    {
                        connectingClients[$"{clientIp}:{port}"]?.Close();
                    }
                    catch
                    {
                        // ignored
                    }
                    connectingClients.TryRemove($"{clientIp}:{port}", out SocketClientInfo clientInfo);
                    Utilities.WriteDebugLog($"{clientIp}:{port} is existed, close it first. Connections count: {connectingClients.Count}");
                }

                try
                {
                    var socketClient = new SocketClientInfo(client);
                    var success = connectingClients.TryAdd($"{clientIp}:{port}", socketClient);
                    if (!success) // close it
                        socketClient.Close();
                }
                catch (Exception ex)
                {
                    Utilities.WriteDebugLog($"{clientIp}:{port}: {ex.ToString()}");
                }

                Utilities.WriteDebugLog($"{clientIp}:{port} connected to port {Port} on thread {Thread.CurrentThread.ManagedThreadId}. Connections count: {connectingClients.Count}");
                Console.WriteLine($"Server: {clientIp}:{port} connected to port {Port} on thread {Thread.CurrentThread.ManagedThreadId}");

                if (ClientConnected != null)
                {
                    ClientConnected(this, clientIp, port, true);
                }
                Task.Factory.StartNew(() => { HandleNewConnection(client); });
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }
        }

        private object sessionIdIncrementLock = new object();

        private void HandleNewConnection(TcpClient client)
        {
            Int32 thisSessionId = -1;
            SessionCommunications session = null;

            if (thisSessionId == -1)
            {
                lock (sessionIdIncrementLock)
                {
                    thisSessionId = newSessionId;
                    newSessionId += 1;
                }
            }

            Thread newSession = new Thread(Run);
            session = new SessionCommunications(client, thisSessionId);
            newSession.IsBackground = true;
            newSession.Name = "Server Session #" + thisSessionId;
            newSession.Start(session);

            SessionCollection.AddSession(session);
        }

        private void Run(object _session)
        {
            SessionCommunications session = (SessionCommunications)_session;

            TcpClient Server = default(TcpClient);
            NetworkStream Stream = default(NetworkStream);
            //IPEndPoint IpEndPoint = default(IPEndPoint);
            byte[] tmp = new byte[2];
            string clientIp = "";
            int port = 0;

            try
            {
                // Create a local Server and Stream objects for clarity.
                Server = session.theClient;
                Stream = Server.GetStream();
            }
            catch (Exception ex)
            {
                // An unexpected WriteErrorLog.
                Utilities.WriteDebugLog(ex.ToString());
                return;
            }

            try
            {
                // Get the remote machine's IP address.
                IPEndPoint endpoint = (IPEndPoint)Server.Client.RemoteEndPoint;
                session.remoteIpAddress = endpoint.Address;
                clientIp = endpoint.Address.ToString();
                port = endpoint.Port;

                // no delay on partially filled packets...
                // Send it all as fast as possible.
                Server.NoDelay = true;
                session.IsRunning = true;



                // Start the communication loop
                byte[] message = new byte[4096];
                int bytesRead;

                do
                {
                    bytesRead = 0;
                    try
                    {
                        bytesRead = Stream.Read(message, 0, message.Length);
                    }
                    catch
                    {
                        break;
                    }

                    if (bytesRead == 0)
                        break;

                    //clientStream.Flush();

                    if (connectingClients.ContainsKey($"{clientIp}:{port}"))
                    {
                        connectingClients[$"{clientIp}:{port}"].ConnectedTime = DateTime.Now;
                    }

                    byte[] data = new byte[bytesRead];

                    Array.Copy(message, data, bytesRead);
                    //string msg = Encoding.UTF8.GetString(data);
                    //Utilities.WriteDebugLog($"Server_Received_{clientIp}:{port}: {msg}");
                    var mgs = Encoding.UTF8.GetString(data);
                    var msgarr = mgs.Split(';');
                    if (Encoding.UTF8.GetString(data).StartsWith("msg.healthcheck()"))
                    {
                        SendToClient(clientIp, port, Encoding.UTF8.GetBytes("msg.healthcheck()"));
                    }
                    else if (Encoding.UTF8.GetString(data).StartsWith("msg.comcheck()"))
                    {
                        GetAllComPort(clientIp, port);
                    }
                    else
                    {
                        Console.WriteLine($"Server_Received_{clientIp}:{port}: {mgs}");

                        EntityBox entityBox = new EntityBox
                        {
                            COM = Convert.ToInt32(msgarr[0]),
                            BOXID = (msgarr[1]),
                            TRAYID = Convert.ToInt32(msgarr[2])
                        };
                        GetInfoOpenBox(entityBox, clientIp, port);

                        if (DataReceived != null)
                            DataReceived(this, clientIp, port, data, bytesRead);
                    }

                } while (true);
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }

            try
            {
                Stream?.Close();
                Stream?.Dispose();
                Server?.Close();

                try
                {
                    connectingClients[$"{clientIp}:{port}"]?.Close();
                }
                catch
                {
                    // ignored
                }

                //if (connectingClients.ContainsKey($"{clientIp}:{port}"))
                //    connectingClients.TryRemove($"{clientIp}:{port}", out SocketClientInfo clientInfo);

                Utilities.WriteDebugLog($"{clientIp}:{port} disconnected on port {Port} on thread {Thread.CurrentThread.ManagedThreadId}");
                if (ClientDisconnected != null)
                {
                    ClientDisconnected(this, clientIp, port);
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog(ex.ToString());
            }

            session.IsRunning = false;
        }

        private void GetAllComPort(string clientIp, int port)
        {
            string[] ports = SerialPort.GetPortNames();
            string allPorts = String.Join("|",ports);
            SendToClient(clientIp, port, Encoding.UTF8.GetBytes(allPorts));
        }
        void GetInfoOpenBox(EntityBox entityBox, string clientIp, int port)
        {
            var _modbus = new modbus();
            string msgReceived = null;

            //Console.WriteLine($"modbus: {_modbus.sp.IsOpen}");
            if (AppSettings.WriteConsoleOrLog == 0)
            {
                Console.WriteLine($"Modbus_Open: {_modbus.sp.IsOpen}");
            }
            else if (AppSettings.WriteConsoleOrLog == 1)
            {
                Utilities.WriteDebugLog("Modbus_Open", $"{_modbus.sp.IsOpen}");
            }
            else
            {
                Console.WriteLine($"Modbus_Open: {_modbus.sp.IsOpen}");
                Utilities.WriteDebugLog("Modbus_Open", $"{_modbus.sp.IsOpen}");
            }
            #region [OpenModbus]
            if (!_modbus.sp.IsOpen)
            {
                bool isOpen = _modbus.Open($"COM{entityBox.COM}", 9600, 8, System.IO.Ports.Parity.Even, System.IO.Ports.StopBits.One);

                if (!isOpen)
                {
                    msgReceived = "DK_com_false";
                    SendToClient(clientIp, port, Encoding.UTF8.GetBytes(msgReceived));

                    if (AppSettings.WriteConsoleOrLog == 0)
                    {
                        Console.WriteLine($"Server_Send_{msgReceived}");
                    }
                    else if (AppSettings.WriteConsoleOrLog == 1)
                    {
                        Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                    }
                    else
                    {
                        Console.WriteLine($"Server_Send_{msgReceived}");
                        Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                    }

                }

            }
            if (_modbus.sp.IsOpen)
            {
                if (AppSettings.WriteConsoleOrLog == 0)
                {
                    Console.WriteLine($"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                }
                else if (AppSettings.WriteConsoleOrLog == 1)
                {
                    Utilities.WriteDebugLog("Check_Com: ", $"{_modbus.sp.PortName} == COM{ entityBox.COM}");

                }
                else
                {
                    Console.WriteLine($"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                    Utilities.WriteDebugLog("Check_Com: ", $"{_modbus.sp.PortName} == COM{ entityBox.COM}");
                }

                try
                {
                    if (_modbus.sp.PortName == $"COM{entityBox.COM}")
                    {
                        bool openSuccess = _modbus.Openbox(Convert.ToByte(entityBox.BOXID), Convert.ToByte(entityBox.TRAYID));
                        if (openSuccess)
                        {

                            msgReceived = "DK_opentray_true";
                            SendToClient(clientIp, port, Encoding.UTF8.GetBytes(msgReceived));

                            if (AppSettings.WriteConsoleOrLog == 0)
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                            }
                            else if (AppSettings.WriteConsoleOrLog == 1)
                            {
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                            }
                            else
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                            }

                            _modbus.sp.Close();

                        }
                        else
                        {

                            msgReceived = "DK_opentray_false";
                            SendToClient(clientIp, port, Encoding.UTF8.GetBytes(msgReceived));

                            if (AppSettings.WriteConsoleOrLog == 0)
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                            }
                            else if (AppSettings.WriteConsoleOrLog == 1)
                            {
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                            }
                            else
                            {
                                Console.WriteLine($"Server_Send_{msgReceived}");
                                Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                            }

                            _modbus.sp.Close();

                        }
                    }
                    else
                    {
                        msgReceived = "DK_openbox_false";
                        SendToClient(clientIp, port, Encoding.UTF8.GetBytes(msgReceived));

                        if (AppSettings.WriteConsoleOrLog == 0)
                        {
                            Console.WriteLine($"Server_Send_{msgReceived}");
                        }
                        else if (AppSettings.WriteConsoleOrLog == 1)
                        {
                            Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");

                        }
                        else
                        {
                            Console.WriteLine($"Server_Send_{msgReceived}");
                            Utilities.WriteDebugLog("Server_Send", $"{msgReceived}");
                        }

                        _modbus.sp.Close();
                    }
                }
                catch (Exception e)
                {
                    if (AppSettings.WriteConsoleOrLog == 0)
                    {
                        Console.WriteLine($"GetInfoOpenBox: {e}");
                    }
                    else if (AppSettings.WriteConsoleOrLog == 1)
                    {
                        Utilities.WriteErrorLog("GetInfoOpenBox", e.ToString());

                    }
                    else
                    {
                        Console.WriteLine($"GetInfoOpenBox: {msgReceived}");
                    Utilities.WriteErrorLog("GetInfoOpenBox", e.ToString());
                    }

                }



            }
            #endregion
        }

        public bool SendToClient(string clientIp, int port, byte[] data)
        {
            if (connectingClients.ContainsKey($"{clientIp}:{port}"))
            {
                try
                {
                    TcpClient client = connectingClients[$"{clientIp}:{port}"].Client;
                    if (client != null && client.Connected)
                    {
                        //Utilities.WriteDebugLog($"Server_Send_{clientIp}:{port}: {Encoding.UTF8.GetString(data)}");
                        NetworkStream clientStream = client.GetStream();
                        clientStream.Write(data, 0, data.Length);
                        return true;
                    }
                }
                catch (Exception e)
                {
                    if (AppSettings.WriteConsoleOrLog == 0)
                    {
                        Console.WriteLine($"SendToClient: {e}");
                    }
                    else if (AppSettings.WriteConsoleOrLog == 1)
                    {
                        Utilities.WriteErrorLog("SendToClient", e.ToString());

                    }
                    else
                    {
                        Console.WriteLine($"SendToClient: {e}");
                        Utilities.WriteErrorLog("SendToClient", e.ToString());
                    }
                    return false;

                }
            }
            return false;
        }

        private void CheckClientConnection(Object stateInfo)
        {
            try
            {
                if (_restarting)
                    return;

                if (connectingClients.Count > _maxConnection)
                {
                    Utilities.WriteDebugLog($"Over max {_maxConnection} connection. Restarting server...");
                    Restart();
                    Utilities.WriteDebugLog($"Server restarted!");
                    return;
                }

                List<string> removeKeys = new List<string>();
                foreach (string key in connectingClients.Keys)
                {
                    try
                    {
                        if (connectingClients.ContainsKey(key)
                            && connectingClients[key].ConnectedTime.AddMilliseconds(_connectionTimeout) < DateTime.Now)
                        {
                            try
                            {
                                // qua thoi gian xac minh, dong ket noi
                                //if (connectingClients[key].Client.Connected)
                                connectingClients[key]?.Close();
                            }
                            catch
                            {

                            }
                            removeKeys.Add(key);
                            //Utilities.Info($"{key} is inactive too long, close and remove it.");
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utilities.WriteErrorLog(ex.ToString(), $"CheckClientConnection_ConnectedTime_{key}");
                    }
                }

                foreach (string key in removeKeys)
                    connectingClients.TryRemove(key, out SocketClientInfo clientInfo);

                Utilities.WriteDebugLog($"Clear connection. Connections count: {connectingClients.Count}");

                try
                {
                    GC.Collect();
                }
                catch
                {

                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog(ex.ToString(), "CheckClientConnection");
            }
        }

        public class SocketClientInfo
        {
            public string Key { get; set; }
            public TcpClient Client { get; set; }
            public DateTime LastHealthCheck { get; set; }
            public DateTime ConnectedTime { get; set; }

            public SocketClientInfo(TcpClient client)
            {
                Client = client;
                LastHealthCheck = DateTime.Now;
                ConnectedTime = DateTime.Now;
            }

            public void Close()
            {
                try
                {
                    try
                    {
                        NetworkStream stream = Client?.GetStream();
                        stream?.Close();
                        stream?.Dispose();
                    }
                    catch
                    {
                        // ignored
                    }
                    Client?.Close();
                    Client?.Dispose();
                    Client = null;

                    GC.Collect();
                }
                catch
                {

                }
            }
        }

        public class SessionCommunications
        {
            public TcpClient theClient;
            public bool IsRunning = false;
            public IPAddress remoteIpAddress;
            public Int32 sessionID;
            public bool disConnect = false;
            public bool paused;
            public bool pauseSent;

            public bool shuttingDown;

            public SessionCommunications(TcpClient _theClient, Int32 _sessionID)
            {
                theClient = _theClient;
                sessionID = _sessionID;
                paused = false;
                pauseSent = false;
                shuttingDown = false;
            }

            // Optional ByVal wait As Int32 = 500
            public void Close()
            {
                Thread bgThread = new Thread(WaitClose);
                bgThread.Start();
            }


            private void WaitClose()
            {
                shuttingDown = true;
                disConnect = true;
                try
                {
                    theClient?.Close();
                }
                catch (Exception ex)
                {
                    Utilities.WriteDebugLog(ex.ToString());
                }
            }
        }
        private class Sessions
        {
            private List<SessionCommunications> sessionCollection = new List<SessionCommunications>();
            private object sessionLockObject = new object();

            public void AddSession(SessionCommunications theNewSession)
            {
                Task.Factory.StartNew(() => { bgAddSession(theNewSession); });
            }

            private void bgAddSession(SessionCommunications theNewSession)
            {
                lock (sessionLockObject)
                {
                    if (sessionCollection.Count > theNewSession.sessionID)
                    {
                        sessionCollection[theNewSession.sessionID] = null;
                        sessionCollection[theNewSession.sessionID] = theNewSession;
                    }
                    else
                    {
                        sessionCollection.Add(theNewSession);
                    }
                }
            }


            public bool GetSession(Int32 sessionID, ref SessionCommunications session)
            {
                try
                {
                    session = sessionCollection[sessionID];
                    if (session == null)
                        return false;
                    if (!session.IsRunning)
                        return false;
                    return true;
                }
                catch (Exception ex)
                {
                    Utilities.WriteDebugLog(ex.ToString());
                    return false;
                }
            }

            public List<SessionCommunications> GetSessionCollection()
            {
                List<SessionCommunications> thisCopy = new List<SessionCommunications>();

                lock (sessionLockObject)
                {
                    for (Int32 i = 0; i <= sessionCollection.Count - 1; i++)
                    {
                        thisCopy.Add(sessionCollection[i]);
                    }
                }

                return thisCopy;
            }

            public void ShutDown()
            {
                lock (sessionLockObject)
                {
                    foreach (SessionCommunications session in sessionCollection)
                    {
                        try
                        {
                            if (session != null && session.IsRunning)
                                session.Close();
                        }
                        catch (Exception ex)
                        {
                            Utilities.WriteDebugLog(ex.ToString());
                        }
                    }
                }
            }
        }
    }
}
