﻿using System;
using System.IO;
using Topshelf;

namespace TN.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HostFactory.Run(x =>
                {
                    x.Service<Startup>(s =>
                    {
                        s.ConstructUsing(name => new Startup());
                        s.WhenStarted(tc => tc.Start());
                        s.WhenStopped(tc => tc.Stop());
                    });

                    x.RunAsLocalSystem();
                    x.SetDescription(AppSettings.ServiceDescription);
                    x.SetDisplayName(AppSettings.ServiceDisplayName);
                    x.SetServiceName(AppSettings.ServiceName);
                });
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Main", ex.ToString());
            }
        }
    }
}
