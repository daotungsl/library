﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using TN.Shared.Service;
using TN.Socket;

namespace TN.Service
{
    public class Startup
    {
        public static SocketServer server;
        private static RecognitionService _recognitionService = new RecognitionService();
        public void Start()
        {
            try
            {
                CheckCom();
                Utilities.WriteOperationLog("Start", "Start");

                //server = new SocketServer(AppSettings.SocketServerIP, AppSettings.SocketServerPort, AppSettings.SocketConnectionTimeout)
                //{
                //    DataReceived = new SocketServer.DataReceivedHandler(DataReceived)
                //};
                //server.Start();
                //SyncJobScheduler.Start();

                _recognitionService.StartServerLibrary();

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Startup.Start", ex.ToString());
            }
        }
        public void Stop()
        {
            try
            {
                _recognitionService.StopLibrary();

                Utilities.WriteOperationLog("Stop", "Stop");
                //SyncJobScheduler.Stop();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Startup.Stop", ex.ToString());
            }
        }

        void DataReceived(SocketServer listener, string clientIp, int port, byte[] receivedData, int byteCount)
        {
            try
            {
                if (byteCount < 4)
                    return;
                string data = Encoding.UTF8.GetString(receivedData).Trim();
                
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("DataReceived", ex.ToString());
            }
        }
        private void CheckCom()
        {
            string[] ports = SerialPort.GetPortNames();

            foreach (string port in ports)
            {
                Console.WriteLine($"{ port} is active");
                Utilities.WriteDebugLog($"{port} is active");
            }

        }

    }
}