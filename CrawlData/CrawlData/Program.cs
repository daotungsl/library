﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LibraryV2.Factory;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace CrawlData
{
    class Program
    {
        static List<object> items = new List<object>();
        static List<string> listItemSingerString = new List<string>();
        static List<string> listItemComposerString = new List<string>();
        static List<GroupSinger> listItemSinger = new List<GroupSinger>();
        static List<GroupComposed> listItemComposer = new List<GroupComposed>();
        static int countPageSinger = 1;
        static int countItemSinger = 1;
        static int countItemComposer = 1;
        static int timeSleep = 10;
        static string nameFileSinger = "Singer.txt";
        static string nameFileComposer = "Composer.txt";
        private static HtmlDocument document = null;
        private static HtmlWeb htmlWeb = null;

        static void Main(string[] args)
        {
            htmlWeb = new HtmlWeb()
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8  //Set UTF8 để hiển thị tiếng Việt
            };
            Console.OutputEncoding = Encoding.UTF8;

            //CrawlComposer();
            CrawlSinger();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Tổng số trang: {countPageSinger}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Tổng số item: {countItemSinger}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Tất cả bản ghi được lưu ra file text tại path: {Directory.GetCurrentDirectory() + "\\" + nameFileSinger}");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Tổng số item Composer: {countItemComposer}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Tất cả bản ghi được lưu ra file text tại path: {Directory.GetCurrentDirectory() + "\\" + nameFileComposer}");

            Console.ReadLine();
        }

        private static void CrawlSinger()
        {

            //Load trang web, nạp html vào document
            LoadItemsSinger("https://nhac.vn/nghe-si?p=");
            if (listItemSinger != null)
            {
                _ = CsvDTO.Write(listItemSinger);

               // WriteText(listItemSingerString, nameFileSinger);

            }
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Tổng số trang: {countPageSinger}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Tổng số item: {countItemSinger}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Tất cả bản ghi được lưu ra file text tại path: {Directory.GetCurrentDirectory() + "\\" + nameFileSinger}");

        }
        private static void CrawlComposer()
        {

            //Load trang web, nạp html vào document
            LoadItemComposer("https://vi.wikipedia.org/wiki/Danh_sách_nhạc_sĩ_tân_nhạc_Việt_Nam");


            if (listItemComposer != null)
            {
               _ = CsvDTO.Write(listItemComposer);

                //WriteText(listItemComposerString, nameFileComposer);
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Tổng số item Composer: {countItemComposer}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Tất cả bản ghi được lưu ra file text tại path: {Directory.GetCurrentDirectory() + "\\" + nameFileComposer}");

        }


        private static void LoadItemComposer(string link)
        {
            try
            {


                document = htmlWeb.Load(link);
                //Load các tag li trong tag ul
                var threadItems = document.DocumentNode.Descendants("table").Where(node =>
                    node.Attributes.Contains("class") && node.Attributes["class"].Value == "wikitable").ToList();
                foreach (var node in threadItems)
                {
                    try
                    {
                        var listRow = node.Descendants("a").ToList();
                        foreach (var htmlNode in listRow)
                        {
                            try
                            {
                                var name = htmlNode.InnerText.Contains('(') ? htmlNode.InnerText.Split('(')[0] : htmlNode.InnerText;

                                if (listItemComposerString.FirstOrDefault(n => n == name) == null && !name.Contains("&"))
                                {
                                    Thread.Sleep(timeSleep);
                                    listItemComposerString.Add(name);
                                    var compose = new GroupComposed();
                                    compose.Id = countItemComposer;
                                    compose.Name = name;
                                    compose.CreatedDate = DateTime.Now;
                                    compose.CreatedUser = 1;
                                    compose.FriendlyName = FactoryFunction.GetFriendlyName(name);
                                    listItemComposer.Add(compose);
                                    Console.WriteLine($"{countItemComposer} | {name}");
                                    countItemComposer++;
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                var threadItems2 = document.DocumentNode.Descendants("div").First(node =>
                    node.Attributes.Contains("class") && node.Attributes["class"].Value == "mw-parser-output")
                    .ChildNodes.Where(node => node.Name == "ul").ToList();
                foreach (var htmlNode in threadItems2)
                {
                    try
                    {
                        var listLi = htmlNode.Descendants("li").ToList();
                        foreach (var node in listLi)
                        {
                            try
                            {
                                var name = node.InnerText.Contains('(') ? node.InnerText.Split('(')[0] : node.InnerText;
                                if (listItemComposerString.FirstOrDefault(n => n == name) == null && !name.Contains("&"))
                                {
                                    Thread.Sleep(timeSleep);
                                    listItemComposerString.Add(name);
                                    var compose = new GroupComposed();
                                    compose.Id = countItemComposer;
                                    compose.Name = name;
                                    compose.CreatedDate = DateTime.Now;
                                    compose.CreatedUser = 1;
                                    compose.FriendlyName = FactoryFunction.GetFriendlyName(name);
                                    listItemComposer.Add(compose);
                                    Console.WriteLine($"{countItemComposer} | {name}");
                                    countItemComposer++;
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Trang: {countItemComposer}, Link: {link}");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Dừng 2 giây tránh block ip :))");
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(2 * 1000);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private static void LoadItemsSinger(string link)
        {
            try
            {

                while (true)
                {
                    document = htmlWeb.Load(link + countPageSinger);
                    //Load các tag li trong tag ul
                    var threadItems = document.DocumentNode.Descendants("ul").First(node =>
                            node.Attributes.Contains("class") && node.Attributes["class"].Value == "list_playlist")
                        .ChildNodes.Where(node => node.Name == "li").ToList();
                    foreach (var item in threadItems)
                    {
                        var linkNode = item.Descendants("h2").First(node =>
                            node.Attributes.Contains("class") && node.Attributes["class"].Value.Contains("name"));
                        //var link = linkNode.Attributes["href"].Value;
                        var name = linkNode.InnerText.Contains('(') ? linkNode.InnerText.Split('(')[0] : linkNode.InnerText;
                        //var readCount = item.Descendants("b").First().InnerText;
                        if (listItemSingerString.FirstOrDefault(n => n == name) == null && !name.Contains("&"))
                        {
                            Thread.Sleep(timeSleep);
                            listItemSingerString.Add(name);
                            var singer = new GroupSinger
                            {
                                Id = countItemSinger,
                                Name = name,
                                CreatedDate = DateTime.Now,
                                CreatedUser = 1,
                                FriendlyName = FactoryFunction.GetFriendlyName(name),

                            };
                            listItemSinger.Add(singer);
                            Console.WriteLine($"{countItemSinger} | {name}");
                            countItemSinger++;
                        }

                        //items.Add(new { text, readCount, link });
                    }

                    if (countPageSinger == 95)
                    {
                        break;
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Trang: {countPageSinger}, Link: {link + countPageSinger}");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Dừng 2 giây tránh block ip :))");
                    Console.ForegroundColor = ConsoleColor.White;

                    countPageSinger++;
                    
                    Thread.Sleep(2 * 1000);
                }
               

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private static void LoadTagUseNodes()
        {
            var threadItems = document.DocumentNode.SelectNodes("//ul[@id='threads']/li").ToList();


            foreach (var item in threadItems)
            {
                //Extract các giá trị từ các tag con của tag li
                var linkNode = item.SelectSingleNode(".//a[contains(@class,'title')]");
                var link = linkNode.Attributes["href"].Value;
                var text = linkNode.InnerText;
                var readCount = item.SelectSingleNode(".//div[@class='folTypPost']/ul/li/b").InnerText;

                items.Add(new { text, readCount, link });
            }
        }

        private static void LoadTagUseLinq()
        {
            var threadItems = document.DocumentNode.Descendants("ul")
                .First(node => node.Attributes.Contains("id") && node.Attributes["id"].Value == "threads")
                .ChildNodes.Where(node => node.Name == "li").ToList();



            foreach (var item in threadItems)
            {
                var linkNode = item.Descendants("a").First(node =>
                    node.Attributes.Contains("class") && node.Attributes["class"].Value.Contains("title"));
                var link = linkNode.Attributes["href"].Value;
                var text = linkNode.InnerText;
                var readCount = item.Descendants("b").First().InnerText;

                items.Add(new { text, readCount, link });
            }
        }

        private static void WriteText(List<string> itemString, string nameFile)
        {
            var path = Directory.GetCurrentDirectory();

            var fileName = path + "\\" + nameFile;
            try
            {
                // Check if file already exists. If yes, delete it.     
                if (!File.Exists(fileName))
                {
                    // Create a new file     
                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        sw.WriteLine("Count|Name");
                    }
                }

                foreach (var e in itemString)
                {
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        if (nameFile == nameFileSinger)
                        {
                            sw.WriteLine($"{itemString.IndexOf(e) + 1 }|{e}");
                            //Console.WriteLine($"{countItemSinger} | {e}");
                            //countItemSinger++;
                        }
                        else
                        {
                            sw.WriteLine($"{itemString.IndexOf(e) + 1 }|{e}");
                            //Console.WriteLine($"{countItemComposer} | {e}");
                            //countItemComposer++;
                        }

                    }
                }


                // Write file contents on console.     
                //using (StreamReader sr = File.OpenText(fileName))
                //{
                //    string s = "";
                //    while ((s = sr.ReadLine()) != null)
                //    {
                //        Console.WriteLine(s);
                //    }
                //}
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }
}
