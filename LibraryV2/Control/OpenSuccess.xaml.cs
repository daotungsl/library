﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TN.Shared.DTO;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;
using LibraryV2.Factory;
using ComboBox = System.Windows.Controls.ComboBox;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using RichTextBox = System.Windows.Controls.RichTextBox;
using TextBox = System.Windows.Controls.TextBox;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;
using LibraryV2.Entity;
using TN.LibraryV2;
using Brushes = System.Windows.Media.Brushes;
using Button = System.Windows.Controls.Button;
using Color = System.Drawing.Color;
using HorizontalAlignment = System.Windows.HorizontalAlignment;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for AddDisk.xaml
    /// </summary>
    public partial class OpenSuccess : Window
    {

        private int _Id = 0;
        private int _xBox = 10;
        private int _yBox = 10;
        private int _xTray = 5;
        private int _yTray = 5;
        private int CountExit = 10;
        private readonly int _boxOpen = 0;
        private readonly int _trayOpen = 0;

        public OpenSuccess(int boxOpen, int trayOpen)
        {
            InitializeComponent();
            _boxOpen = boxOpen;
            _trayOpen = trayOpen;
            CreateViewBtn();

            Task.Run(() =>
            {

                while (CountExit > -1)
                {
                    if (BtnClose.Dispatcher.CheckAccess())
                    {
                        BtnClose.Content = $"Thoát({CountExit})";
                    }
                    else
                    {
                        BtnClose.Dispatcher.Invoke(DispatcherPriority.Normal,
                            new Action(() =>
                            {
                                BtnClose.Content = $"Thoát({CountExit})";
                            }));
                    }
                    Thread.Sleep(1000);
                    CountExit--;
                }
                if (this.Dispatcher.CheckAccess())
                {
                    this.Close();
                }
                else
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action(() =>
                        {
                            this.Close();
                        }));
                }

            });



        }

        private void CreateViewBtn()
        {
            int count = 1;

            int column = AppSetting.BoxColumn;
            int row = AppSetting.BoxRow;
            if (AppSetting.IsDirectionBox)
            {
                count = column * row;
            }
            GenerateBox(column, row, count);
            
            GenerateTray(10);
        }

        private void GenerateBox(int column, int row, int count)
        {
            int recBoxH = Convert.ToInt32(RecBox.Height);
            int recBoxW = Convert.ToInt32(RecBox.Width);
            int boxH = (recBoxH - 10 - row * 10) / row;
            int boxW = (recBoxW - 10 - column * 10) / column;
            _xBox = 20;
            _yBox = 82;
            int direction = 1;
            for (int i = 1; i <= column; i++)
            {
                if (AppSetting.IsDirectionBox)
                {
                    direction = (column * row) - (row * i) + 1;
                }
                
                for (int j = 1; j <= row; j++)
                {
                    CreateBtnBox(_xBox, _yBox, boxW, boxH, direction, (column * row));
                    _yBox += boxH + 10;
                    direction++;
                }
                _xBox += boxW + 10;
                _yBox = 82;
            }
        }

        private void GenerateTray(int count)
        {
            int recBoxH = Convert.ToInt32(RecTray.Height);
            int recBoxW = Convert.ToInt32(RecTray.Width);
            int trayH = (recBoxH - 10 - 1 * 10) / 1;
            int trayW = (recBoxW - 10 - 10 * 10) / 10;
            _xTray = 520;
            _yTray = 328;
            int direction = 1;
            if (AppSetting.IsDirectionTray)
            {
                direction = count;
            }
            for (int i = 1; i <= count; i++)
            {
                CreateBtnTray(_xTray, _yTray, trayW, trayH, direction);
                _xTray += trayW + 10;
                if (AppSetting.IsDirectionTray)
                {
                    direction--;
                }
                else
                {
                    direction++;
                }
            }
        }
        private void CreateBtnBox(int x, int y, int boxW, int boxH, int num, int controlTxt)
        {
            // Creating and initializing a new SimpleButton control 
            Button simpleButton = new Button();

            if (controlTxt > 39 && controlTxt < 99)
            {
                simpleButton.Content = $@"N   {num:D2}";
            }
            else if (controlTxt > 99)
            {
                simpleButton.Content = $@"{num:D3}";
            }
            else
            {
                simpleButton.Content = $@"NGĂN {num:D2}";
            }



            simpleButton.Width = boxW;
            simpleButton.Height = boxH;
            simpleButton.HorizontalAlignment = HorizontalAlignment.Left;
            simpleButton.VerticalAlignment = VerticalAlignment.Top;
            simpleButton.FontSize = 16;

            simpleButton.Margin = new Thickness(x, y, 0, 0);
            Tag = num;
            if (_boxOpen == num)
            {

                simpleButton.FontWeight = FontWeights.SemiBold;
                simpleButton.FontSize = 20;
                simpleButton.Background = Brushes.Green;

            }
            GridMain.Children.Add(simpleButton);
        }
        private void CreateBtnTray(int x, int y, int TrayW, int TrayH, int num)
        {
            // Creating and initializing a new SimpleButton control 
            Button simpleButton = new Button();
            simpleButton.Content = $"Ô\n{num:D2}";
            simpleButton.HorizontalAlignment = HorizontalAlignment.Left;
            simpleButton.VerticalAlignment = VerticalAlignment.Top;
            simpleButton.HorizontalContentAlignment = HorizontalAlignment.Center;
            simpleButton.VerticalContentAlignment = VerticalAlignment.Center;
            simpleButton.FontSize = 16;
            simpleButton.Width = TrayW;
            simpleButton.Height = TrayH;
            simpleButton.Margin = new Thickness(x, y, 0, 0);

            if (_trayOpen == num)
            {
                simpleButton.FontWeight = FontWeights.SemiBold;
                simpleButton.FontSize = 20;
                simpleButton.Background = Brushes.Green;
            }
            GridMain.Children.Add(simpleButton);
        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {

            Close();
        }


    }
}
