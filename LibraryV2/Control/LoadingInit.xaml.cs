﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for LoadingInit.xaml
    /// </summary>
    public partial class LoadingInit : Window
    {
        private DispatcherTimer dispatcherTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 10), IsEnabled = true };
        private int count = 5 * 100;
        private int countOpacity = 10;

        public LoadingInit()
        {
            InitializeComponent();
            MainLoading.Opacity = 0;
            //Hidden Item
            ImgConnect.Visibility = Visibility.Hidden;
            ImgInit.Visibility = Visibility.Hidden;
            dispatcherTimer.Tick += new EventHandler(ShowTimer_Tick);
        }

        private void ShowTimer_Tick(object sender, EventArgs e)
        {
            LblTime.Content = DateTime.Now.ToString(" dd/MM/yyyy - HH:mm:ss tt");
            if (MainLoading.Opacity != 1)
            {
                MainLoading.Opacity += 0.01;
            }
            if (count < 150)
            {
                MainLoading.Opacity -= 0.05;
            }
            if (count == 350)
            {
                ImgConnect.Visibility = Visibility.Visible;
                ImgConnectLoad.Visibility = Visibility.Hidden;
                
            }

           
            if (count == 250)
            {
                ImgInit.Visibility = Visibility.Visible;
                ImgInitLoad.Visibility = Visibility.Hidden;
            }
            

            if (count == 0)
            {
                dispatcherTimer.IsEnabled = false;
                Close();
            }

            count--;
        }


    }
}
