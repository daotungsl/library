﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TN.Shared.DTO;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;
using LibraryV2.Factory;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO.Ports;
using LibraryV2.Entity;
using Newtonsoft.Json;
using TN.LibraryV2;
using TN.Shared.Entity;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Drawing.Brushes;
using Color = System.Windows.Media.Color;
using ComboBox = System.Windows.Controls.ComboBox;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for AddDisk.xaml
    /// </summary>
    public partial class Configuration : Window
    {

        public Configuration()
        {
            InitializeComponent();
            //Hidden item

            SetItemForGlobal();
            LoadContent();



        }

        private void SetItemForGlobal()
        {
            CostGlobal.CbbComBlock1 = CbbComBlock1;
            CostGlobal.CbbComBlock2 = CbbComBlock2;
            CostGlobal.LblComBlock1 = LblComBlock1;
            CostGlobal.LblComBlock2 = LblComBlock2;
        }

        private void LoadContent()
        {

            var listCbbBox = new List<string>
            {
                "Từ trái sang phải",
                "Từ phải sang trái"
            };

            CbbOrderBox.ItemsSource = listCbbBox;
            CbbOrderBox.SelectedIndex = AppSetting.IsDirectionBox ? 1 : 0;

            CbbOrderTray.ItemsSource = listCbbBox;
            CbbOrderTray.SelectedIndex = AppSetting.IsDirectionTray ? 1 : 0;

            TxtRowBox.Text = AppSetting.BoxRow.ToString();
            TxtColumBox.Text = AppSetting.BoxColumn.ToString();
            TxtRowTray.Text = "10";

            LoadComInDb();
            CheckConnectToService();
            LoadCredential();
        }

        private void CheckConnectToService()
        {

            if (MainWindow.Client.Send("msg.comcheck()"))
            {
                LblStatusConnect.Content = "Online";
                LblStatusConnect.Background = new SolidColorBrush(Color.FromRgb(15, 168, 36));
            }
            else
            {
                LblStatusConnect.Content = "Local";
                LblStatusConnect.Background = new SolidColorBrush(Color.FromRgb(168, 15, 15));
                UsingLocalCom();
            }


        }

        private void UsingLocalCom()
        {
            string[] ports = SerialPort.GetPortNames();
            string allPorts = "COM_Empty";
            if (ports.Length != 0)
            {
                allPorts = String.Join("|", ports);
            }

            MainWindow.MethodCheckCom(allPorts);
        }
        private void LoadComInDb()
        {
            var ComBlock1 = DevicesControllerCsv.GetById(1).COM;
            var ComBlock2 = DevicesControllerCsv.GetById(20).COM;
            LblComBlock1.Content = $"COM{ComBlock1}";
            LblComBlock2.Content = $"COM{ComBlock2}";
        }
        private void LoadCredential()
        {
            var Credential = LoadJson();
            TxtType.Text = Credential.Type;
            TxtProjectId.Text = Credential.Project_id;
            TxtPrivateKeyId.Text = Credential.Private_key_id;
            TxtPrivateKey.Text = Credential.Private_key;
            TxtClientEmail.Text = Credential.Client_email;
            TxtClientId.Text = Credential.Client_id;
            TxtAuthUri.Text = Credential.Auth_uri;
            TxtTokenUri.Text = Credential.Token_uri;
            TxtAuthProviderCertUrl.Text = Credential.auth_provider_x509_cert_url;
            TxtClientCertUrl.Text = Credential.Client_x509_cert_url;
        }
        public CredentialGoogle LoadJson()
        {
            using (StreamReader r = new StreamReader("./Properties/credentials.json"))
            {
                string json = r.ReadToEnd();
                CredentialGoogle items = JsonConvert.DeserializeObject<CredentialGoogle>(json);
                return items;
            }

        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();

        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (CbbComBlock1.SelectedValue.ToString() == CbbComBlock2.SelectedValue.ToString())
                {
                    var nofi = new Notification(TypeNofi.Warning, "Không thể gán 2 Block cùng 1 COM", "Thông báo");
                    nofi.ShowDialog();
                }
                else
                {
                    SaveConfigDirection();

                    SaveConfigCom();

                    SaveCredentialGoogle();

                    var nofi = new Notification(TypeNofi.Success, "Thông tin của bạn đã được lưu lại thành công.", "Thông báo");
                    nofi.ShowDialog();
                    Close();
                }


            }
            catch
            {
                var nofi = new Notification(TypeNofi.Error, "Lỗi hệ thống không lưu lại được cấu hình.", "Thông báo");
                nofi.ShowDialog();
                Close();
            }
        }

        private void SaveConfigDirection()
        {
            FactoryFunction.SaveDirection("IsDirectionBox", $"{CbbOrderBox.SelectedIndex}");
            FactoryFunction.SaveDirection("IsDirectionTray", $"{CbbOrderTray.SelectedIndex}");
        }

        private void SaveConfigCom()
        {
            var listNewDevices = new List<Devices>();
            var selectComBlock1 = CbbComBlock1.SelectedValue.ToString();
            var selectComBlock2 = CbbComBlock2.SelectedValue.ToString();



            foreach (var device in CsvDTO.ListDevices)
            {
                if (device.Id < 11 && selectComBlock1.StartsWith("COM"))
                {
                    device.COM = selectComBlock1.Replace("COM", "");
                }
                else if (selectComBlock2.StartsWith("COM"))
                {
                    device.COM = selectComBlock2.Replace("COM", "");
                }
                listNewDevices.Add(device);

            }
            DevicesControllerCsv.EditList(listNewDevices);

        }
        private void SaveCredentialGoogle()
        {
            CredentialGoogle credential = new CredentialGoogle
            {

                Type = TxtType.Text,
                Project_id = TxtProjectId.Text,
                Private_key_id = TxtPrivateKeyId.Text,
                Private_key = TxtPrivateKey.Text,
                Client_email = TxtClientEmail.Text,
                Client_id = TxtClientId.Text,
                Auth_uri = TxtAuthUri.Text,
                Token_uri = TxtTokenUri.Text,
                auth_provider_x509_cert_url = TxtAuthProviderCertUrl.Text,
                Client_x509_cert_url = TxtClientCertUrl.Text
            };
            var dl = JsonConvert.SerializeObject(credential);
            try
            {
                File.WriteAllText("./Properties/credentials.json", dl);

            }
            catch
            {

            }
        }
    }
}
