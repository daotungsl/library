﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TN.Shared.DTO;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;
using LibraryV2.Factory;
using ComboBox = System.Windows.Controls.ComboBox;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using RichTextBox = System.Windows.Controls.RichTextBox;
using TextBox = System.Windows.Controls.TextBox;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Wpf;
using LibraryV2.Entity;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for AddDisk.xaml
    /// </summary>
    public partial class AddDisk : Window
    {
        private List<GroupItems> _groupItems = null;
        private List<GroupSinger> _groupSinger = null;
        private List<GroupComposed> _groupComposed = null;
        private List<Devices> _devices = null;
        private List<DeviceItemsModel> _deviceItems = null;
        private bool isSnap = false;
        private int _Id = 0;
        public static bool isDelete = false;
        private int NextCamera = 0;

        #region [InitCamera]
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        private FilterInfo _currentDevice;

        #region Private fields

        private IVideoSource _videoSource;

        #endregion


        #endregion

        public AddDisk(int id)
        {
            InitializeComponent();
            _Id = id;
            //Hidden item
            LblSttSnap.Visibility = Visibility.Hidden;
            BtnSnap.Visibility = Visibility.Hidden;
            CbbDeviceItem.Visibility = Visibility.Hidden;
            BtnDelete.Visibility = Visibility.Hidden;
            BtnSwitchCamera.Visibility = Visibility.Hidden;
            LblSttSwitchCamera.Visibility = Visibility.Hidden;
            GetVideoDevices();
            LoadContent();



        }
        private void ImageExit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        #region [HandleCamera]

        private void GetVideoDevices()
        {
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (VideoDevices.Any())
            {
                _currentDevice = VideoDevices[0];
            }

        }
        private void video_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
            try
            {
                BitmapImage bi;
                using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
                {
                    bi = bitmap.ToBitmapImage();
                }
                bi.Freeze(); // avoid cross thread operations and prevents leaks
                Dispatcher.BeginInvoke(new ThreadStart(delegate { ImgVideo.Source = bi; }));
            }
            catch (Exception exc)
            {
                //MessageBox.Show("Error on _videoSource_NewFrame:\n" + exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                StopCamera();
            }

        }

        private void StopCamera()
        {
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
                _videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame);
            }
        }
        private void StartCamera()
        {
            if (_currentDevice != null)
            {
                _videoSource = new VideoCaptureDevice(_currentDevice.MonikerString);
                _videoSource.NewFrame += video_NewFrame;
                _videoSource.Start();
            }
        }
        private void BtnSwitchCamera_Click(object sender, RoutedEventArgs e)
        {
            StopCamera();
                NextCamera++;
                if (NextCamera > VideoDevices.Count - 1)
                {
                    NextCamera = 0;
                }
            _currentDevice = VideoDevices[NextCamera];
            StartCamera();

        }

        #endregion

        #region [Load Content]
        private void LoadContent()
        {

            LoadItemGroup();
            LoadItemSinger();
            LoadItemComposer();
            LoadDevice();

            if (_Id != 0)
            {
                BtnDelete.Visibility = Visibility.Visible;
                var itemEdit = CsvDTO.ListItems.FirstOrDefault(x => x.Id == _Id);
                if (itemEdit != null)
                {
                    TxtNameDisk.Text = itemEdit.Name;
                    CbbGroup.Text = GroupItemsControllerCsv.GetById(itemEdit.GroupItemId).Name;
                    CbbComposer.Text = GroupComposedControllerCsv.GetById(itemEdit.GroupComposedId).Name;
                    CbbSinger.Text = GroupSingerControllerCsv.GetById(itemEdit.GroupSingerId).Name;
                    CbbDevice.Text = DevicesControllerCsv.GetById(itemEdit.DeviceId).Name;
                    LoadDeviceItem(DevicesControllerCsv.GetById(itemEdit.DeviceId).Name);
                    CbbDeviceItem.Text = DeviceItemsControllerCsv.GetById(itemEdit.DeviceItemId).Name;
                    RtxtbDes.Document.Blocks.Clear();
                    RtxtbDes.Document.Blocks.Add(new Paragraph(new Run($"{FactoryFunction.Base64Decode(itemEdit.Description)}")));

                    if (itemEdit.Banner != "")
                    {
                        TxtNameLink.Text = Directory.GetCurrentDirectory() + itemEdit.Banner;
                    }

                    try
                    {
                        ImgVideo.Source = new BitmapImage(new Uri($@"{Directory.GetCurrentDirectory()}{itemEdit.Banner}"));
                    }
                    catch
                    {
                        ImgVideo.Source = new BitmapImage(new Uri("no-image.png", UriKind.Relative));

                    }


                }
            }
        }
        private void LoadItemGroup()
        {
            _groupItems = GroupItemsControllerCsv.GetAll();
            var nameItem = _groupItems.Select(x => x.Name).ToList();
            CbbGroup.ItemsSource = nameItem;
        }
        private void LoadItemSinger()
        {
            _groupSinger = GroupSingerControllerCsv.GetAll();
            var nameItem = _groupSinger.Select(x => x.Name).ToList();

            CbbSinger.ItemsSource = nameItem;
        }
        private void LoadItemComposer()
        {
            _groupComposed = GroupComposedControllerCsv.GetAll();
            var nameItem = _groupComposed.Select(x => x.Name).ToList();

            CbbComposer.ItemsSource = nameItem;
        }
        private void LoadDevice()
        {
            _devices = DevicesControllerCsv.GetAll();
            var nameItem = _devices.Select(x => x.Name).ToList();

            CbbDevice.ItemsSource = nameItem;


        }
        private void LoadDeviceItem(string name)
        {
            _deviceItems = DeviceItemsControllerCsv.GetAllByName(name);
            var nameItem = _deviceItems.Select(x => x.Name).ToList();

            CbbDeviceItem.ItemsSource = nameItem;
        }


        #endregion

        #region [Combobox Handle Search]
        private void CbbGroup_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var cmbx = sender as ComboBox;
            var nameItem = _groupItems.Select(x => x.Name).ToList();
            cmbx.ItemsSource = from item in nameItem
                               where item.ToLower().Contains(cmbx.Text.ToLower())
                               select item;
            cmbx.IsDropDownOpen = true;
        }

        private void CbbSinger_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var cmbx = sender as ComboBox;
            var nameItem = _groupSinger.Select(x => x.Name).ToList();
            cmbx.ItemsSource = from item in nameItem
                               where item.ToLower().Contains(cmbx.Text.ToLower())
                               select item;
            cmbx.IsDropDownOpen = true;
        }

        private void CbbComposer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var cmbx = sender as ComboBox;
            var nameItem = _groupComposed.Select(x => x.Name).ToList();
            cmbx.ItemsSource = from item in nameItem
                               where item.ToLower().Contains(cmbx.Text.ToLower())
                               select item;
            cmbx.IsDropDownOpen = true;
        }

        private void CbbDevice_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var cmbx = sender as ComboBox;
            var nameItem = _devices.Select(x => x.Name).ToList();
            cmbx.ItemsSource = from item in nameItem
                               where item.ToLower().Contains(cmbx.Text.ToLower())
                               select item;
            cmbx.IsDropDownOpen = true;
        }

        private void CbbDeviceItem_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var cmbx = sender as ComboBox;
            var nameItem = _deviceItems.Select(x => x.Name).ToList();
            cmbx.ItemsSource = from item in nameItem
                               where item.ToLower().Contains(cmbx.Text.ToLower())
                               select item;
            cmbx.IsDropDownOpen = true;
        }


        #endregion

        #region [Hover]
        private void TxtNameDisk_LostFocus(object sender, RoutedEventArgs e)
        {
            var cmbx = sender as TextBox;
            if (cmbx.Text == "")
            {
                cmbx.Text = "Nhập tên đĩa";
            }
        }

        private void TxtNameDisk_GotFocus(object sender, RoutedEventArgs e)
        {
            var cmbx = sender as TextBox;
            if (cmbx.Text == "Nhập tên đĩa")
            {
                cmbx.Text = "";
            }
        }

        private void RtxtbDes_LostFocus(object sender, RoutedEventArgs e)
        {
            var cmbx = sender as RichTextBox;
            if (new TextRange(cmbx.Document.ContentStart, cmbx.Document.ContentEnd).Text == "")
            {
                cmbx.Document.Blocks.Clear();
                cmbx.Document.Blocks.Add(new Paragraph(new Run("Miêu tả ...")));
            }
        }

        private void RtxtbDes_GotFocus(object sender, RoutedEventArgs e)
        {
            var cmbx = sender as RichTextBox;
            if (new TextRange(cmbx.Document.ContentStart, cmbx.Document.ContentEnd).Text == "Miêu tả ...\r\n")
            {
                cmbx.Document.Blocks.Clear();
            }
        }


        #endregion



        private void TogCamera_Click(object sender, RoutedEventArgs e)
        {


            if (TogCamera.IsChecked ?? false)
            {
                LblSttTogCamera.Content = "Camera bật";
                LblSttSnap.Visibility = Visibility.Visible;
                BtnSnap.Visibility = Visibility.Visible;
                if (VideoDevices.Count > 1)
                {
                    BtnSwitchCamera.Visibility = Visibility.Visible;
                    LblSttSwitchCamera.Visibility = Visibility.Visible;
                }
                StartCamera();
            }
            else
            {
                LblSttTogCamera.Content = "Camera tắt";
                LblSttSnap.Visibility = Visibility.Hidden;
                BtnSnap.Visibility = Visibility.Hidden;
                BtnSwitchCamera.Visibility = Visibility.Hidden;
                LblSttSwitchCamera.Visibility = Visibility.Hidden;
                if (!isSnap)
                {
                    try
                    {
                        ImgVideo.Source = new BitmapImage(new Uri(TxtNameLink.Text));
                    }
                    catch
                    {
                        ImgVideo.Source = new BitmapImage(new Uri("no-image.png", UriKind.Relative));

                    }

                }

                StopCamera();
            }
        }

        private void BtnSnap_Click(object sender, RoutedEventArgs e)
        {
            if (!isSnap)
            {
                isSnap = true;
                StopCamera();
                imgSnap.Source = new BitmapImage(new Uri("repeat-snap.png", UriKind.Relative));
                LblSttSnap.Content = "Chụp lại";
                GeneratePathFile();
            }
            else
            {
                isSnap = false;
                StartCamera();
                imgSnap.Source = new BitmapImage(new Uri("camera.png", UriKind.Relative));
                LblSttSnap.Content = "Chụp ảnh";
            }
        }
        public string GetFolderPath()
        {
            //FolderBrowserDialog Dialog = new FolderBrowserDialog();
            //while (Dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            //{
            //    Dialog.Reset();
            //}
            //return Dialog.SelectedPath;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = @"Image files (*.jpg)|*.jpg|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var pathFile = openFileDialog.FileName;
                return pathFile;
            }
            else
            {
                return null;
            }

        }

        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {

            var path = GetFolderPath();
            ImgVideo.Source = new BitmapImage(new Uri(path));
            GeneratePathFile();


        }

        private void CbbDevice_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CbbDeviceItem.Visibility = Visibility.Visible;
            LoadDeviceItem(CbbDevice.SelectedValue.ToString());
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            var data = new Items();
            data.Name = FactoryFunction.FirstCharToUpper(TxtNameDisk.Text == "Nhập tên đĩa" ? "" : TxtNameDisk.Text);
            data.FriendlyName = FactoryFunction.GetFriendlyName(TxtNameDisk.Text);
            data.Id = CsvDTO.ListItems?.Count > 0 ? CsvDTO.ListItems.Last().Id + 1 : 1;
            data.Banner = TxtNameLink.Text == "Đường dẫn ảnh" ? "" : TxtNameLink.Text.Replace(Directory.GetCurrentDirectory(), "");
            data.Description = FactoryFunction.Base64Encode(new TextRange(RtxtbDes.Document.ContentStart, RtxtbDes.Document.ContentEnd).Text.Replace("\n", "").Replace("\r", ""));
            data.DeviceId = DevicesControllerCsv.GetByName(CbbDevice.SelectedValue.ToString()).Id;
            data.DeviceItemId = DeviceItemsControllerCsv.GetByOrder(GetDeviceItems(), DevicesControllerCsv.GetByName(CbbDevice.SelectedValue.ToString()).Id).Id;
            data.GroupComposedId = GroupComposedControllerCsv.GetByName(CbbComposer.Text).Id;
            data.GroupItemId = GroupItemsControllerCsv.GetByName(CbbGroup.Text).Id;
            data.GroupSingerId = GroupSingerControllerCsv.GetByName(CbbSinger.Text).Id;
            data.Like = 0;
            data.Note = "";
            if (_Id == 0)
            {
                if (ItemsControllerCsv.Insert(data))
                {

                    
                    SaveImgFile((BitmapImage) ImgVideo.Source, TxtNameLink.Text);
                    
                    var nofi = new Notification(TypeNofi.Success, "Thêm mới đĩa thành công", "Thông báo");
                    nofi.ShowDialog();
                    Close();
                }
                else
                {
                    var nofi = new Notification(TypeNofi.Error, "Thêm mới đĩa không thành công", "Thông báo");
                    nofi.ShowDialog();
                    Close();
                }
            }
            else
            {
                data.Id = _Id;
                data.Like = CsvDTO.ListItems == null ? 0 : CsvDTO.ListItems.First(x => x.Id == _Id).Like;
                if (ItemsControllerCsv.Edit(data))
                {

                    SaveImgFile((BitmapImage)ImgVideo.Source, TxtNameLink.Text);

                    var nofi = new Notification(TypeNofi.Success, "Cập nhật đĩa thành công", "Thông báo");
                    nofi.ShowDialog();
                    Close();
                }
                else
                {
                    var nofi = new Notification(TypeNofi.Error, "Cập nhật đĩa không thành công", "Thông báo");
                    nofi.ShowDialog();
                    Close();
                }
            }

        }

        private int? GetDeviceItems()
        {
            return Convert.ToInt32(CbbDeviceItem.SelectedValue.ToString().Split(' ')[0]);
        }
        private void GeneratePathFile()
        {
            string bannerPath = $@"{Directory.GetCurrentDirectory()}\Data\Image";
            if (!Directory.Exists(bannerPath))
            {
                Directory.CreateDirectory(bannerPath);
            }
            string randomNameImage = RandomString(5);
            string imagePath = bannerPath + "\\" + randomNameImage + ".Jpeg";
            TxtNameLink.Text = imagePath;

        }
        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private void SaveFileToProject(Image image, string imagePath)
        {
            try
            {
                if (imagePath != "" && image != null)
                {
                    image.Save(imagePath, ImageFormat.Jpeg);
                }
            }
            catch
            {

            }


        }
        public static void SaveImgFile(BitmapImage image, string filePath)
        {
            try
            {
                BitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    encoder.Save(fileStream);
                }
            }
            catch
            {
               
            }
        }
        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var confirm = new Notification(TypeNofi.Confirm, "Bạn có chắc chắn muốn xóa ?", "Yêu cầu xác nhận");
            confirm.ShowDialog();

            if (isDelete)
            {
                if (ItemsControllerCsv.Delete(_Id))
                {
                    var nofi = new Notification(TypeNofi.Success, "Xóa đĩa thành công", "Thông báo");
                    nofi.ShowDialog();
                }
                else
                {
                    var nofi = new Notification(TypeNofi.Error, "Xóa đĩa không thành công", "Thông báo");
                    nofi.ShowDialog();
                }
            }


            Close();

        }


    }
}
