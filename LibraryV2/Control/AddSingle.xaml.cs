﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryV2.Entity;
using LibraryV2.Factory;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for AddSingle.xaml
    /// </summary>
    public partial class AddSingle : Window
    {
        public AddSingle(TypeBtnAdd type)
        {
            InitializeComponent();

            GenViewContent(type);
            //Hidden 
            LblError.Visibility = Visibility.Hidden;
        }

        private void GenViewContent(TypeBtnAdd type)
        {

            switch (type)
            {
                case TypeBtnAdd.Singer:
                    LblNameTitle.Content = "Thêm ca sĩ";
                    LblName.Content = "Tên ca sĩ";
                    TxtContent.Text = "Nhập tên ca sĩ";
                    break;
                case TypeBtnAdd.Composer:
                    LblNameTitle.Content = "Thêm nhạc sĩ";
                    LblName.Content = "Tên nhạc sĩ";
                    TxtContent.Text = "Nhập tên nhạc sĩ";
                    break;
                case TypeBtnAdd.Group:
                    LblNameTitle.Content = "Thêm thể loại";
                    LblName.Content = "Tên thể loại";
                    TxtContent.Text = "Nhập tên thể loại";
                    break;
            }

        }
        private void ImageExit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private void InstallComposer()
        {
            var data = new GroupComposed
            {
                Name = FactoryFunction.FirstCharToUpper(TxtContent.Text),
                FriendlyName = FactoryFunction.GetFriendlyName(TxtContent.Text),
                CreatedDate = DateTime.Now,
                CreatedUser = 1,
                Id = CsvDTO.ListGroupComposed.Last().Id + 1
            };

            if (GroupComposedControllerCsv.Insert(data))
            {
                var nofi = new Notification(TypeNofi.Success, "Thêm mới nhạc sĩ thành công", "Thông báo");
                nofi.ShowDialog();
                Close();

            }
            else
            {
                var nofi = new Notification(TypeNofi.Error, "Thêm mới nhạc sĩ không thành công", "Thông báo");
                nofi.ShowDialog();
                Close();
            }
        }
        private void InstallSinger()
        {
            var data = new GroupSinger
            {
                Name = FactoryFunction.FirstCharToUpper(TxtContent.Text),
                FriendlyName = FactoryFunction.GetFriendlyName(TxtContent.Text),
                CreatedDate = DateTime.Now,
                CreatedUser = 1,
                Id = CsvDTO.ListGroupSinger.Last().Id + 1
            };

            
            if (GroupSingerControllerCsv.Insert(data))
            {
                var nofi = new Notification(TypeNofi.Success, "Thêm mới ca sĩ thành công", "Thông báo");
                nofi.ShowDialog();
                Close();

            }
            else
            {
                var nofi = new Notification(TypeNofi.Error, "Thêm mới ca sĩ không thành công", "Thông báo");
                nofi.ShowDialog();
                Close();
            }

        }
        private void InstallGroup()
        {
            var data = new GroupItems
            {
                Name = FactoryFunction.FirstCharToUpper(TxtContent.Text),
                FriendlyName = FactoryFunction.GetFriendlyName(TxtContent.Text),
                CreatedDate = DateTime.Now,
                CreatedUser = 1,
                Id = CsvDTO.ListGroupItems.Count > 0 ? CsvDTO.ListGroupItems.Last().Id + 1 : 1
            };

            if (GroupItemsControllerCsv.Insert(data))
            {
                var nofi = new Notification(TypeNofi.Success, "Thêm mới thể loại thành công", "Thông báo");
                nofi.ShowDialog();
                Close();

            }
            else
            {
                var nofi = new Notification(TypeNofi.Error, "Thêm mới thể loại không thành công", "Thông báo");
                nofi.ShowDialog();
                Close();
            }

        }

        private void TxtContent_TextChanged(object sender, TextChangedEventArgs e)
        {


            
            if (TxtContent.Text == "" || TxtContent.Text.StartsWith("Nhập tên"))
            {
                BtnApply.IsEnabled = false;
            }
            else
            {
                switch (LblNameTitle.Content)
                {
                    case "Thêm ca sĩ":
                        if (CsvDTO.ListGroupSinger.FirstOrDefault(x => x.Name.Equals(FactoryFunction.FirstCharToUpper(TxtContent.Text))) != null)
                        {
                            LblError.Visibility = Visibility.Visible;
                            LblError.Content = "Tên ca sĩ đã tồn tại.";
                            BtnApply.IsEnabled = false;
                        }
                        else
                        {
                            LblError.Visibility = Visibility.Hidden;
                            BtnApply.IsEnabled = true;
                        }
                        break;
                    case "Thêm nhạc sĩ":
                        if (CsvDTO.ListGroupComposed.FirstOrDefault(x => x.Name.Equals(FactoryFunction.FirstCharToUpper(TxtContent.Text))) != null)
                        {
                            LblError.Visibility = Visibility.Visible;
                            LblError.Content = "Tên nhạc sĩ đã tồn tại.";
                            BtnApply.IsEnabled = false;
                        }
                        else
                        {
                            LblError.Visibility = Visibility.Hidden;
                            BtnApply.IsEnabled = true;
                        }
                        break;
                    case "Thêm thể loại":
                        if (CsvDTO.ListGroupItems.FirstOrDefault(x => x.Name.Equals(FactoryFunction.FirstCharToUpper(TxtContent.Text))) != null)
                        {
                            LblError.Visibility = Visibility.Visible;
                            LblError.Content = "Tên thể loại đã tồn tại.";
                            BtnApply.IsEnabled = false;
                        }
                        else
                        {
                            LblError.Visibility = Visibility.Hidden;
                            BtnApply.IsEnabled = true;
                        }
                        break;
                }
            }
        }

        private void TxtContent_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TxtContent.Text.StartsWith("Nhập tên"))
            {
                TxtContent.Text = "";
            }
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            switch (LblNameTitle.Content)
            {
                case "Thêm ca sĩ":
                    InstallSinger();
                    break;
                case "Thêm nhạc sĩ":
                    InstallComposer();
                    break;
                case "Thêm thể loại":
                    InstallGroup();
                    break;
            }
        }
    }
}
