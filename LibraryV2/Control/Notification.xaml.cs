﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryV2.Entity;
using LibraryV2.Factory;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace LibraryV2.Control
{
    /// <summary>
    /// Interaction logic for AddSingle.xaml
    /// </summary>
    public partial class Notification : Window
    {
        public Notification(TypeNofi type, string msg, string title)
        {
            InitializeComponent();

            //Hidden 
            BtnAccess.Visibility = Visibility.Hidden;
            BtnNotAccess.Visibility = Visibility.Hidden;
            //TxtError.Visibility = Visibility.Hidden;
            GenViewContent(type, msg, title);

        }
        private void GenViewContent(TypeNofi type, string msg, string title)
        {

            switch (type)
            {
                case TypeNofi.Warning:
                    TxtError.Foreground = Brushes.Coral;
                    break;
                case TypeNofi.Error:
                    TxtError.Foreground = Brushes.IndianRed;
                    break;
                case TypeNofi.Information:
                    TxtError.Foreground = Brushes.CornflowerBlue;
                    break;
                case TypeNofi.Success:
                    TxtError.Foreground = Brushes.LightGreen;
                    break;
                case TypeNofi.Confirm:
                    TxtError.Foreground = Brushes.IndianRed;
                    BtnAccess.Visibility = Visibility.Visible;
                    BtnNotAccess.Visibility = Visibility.Visible;
                    BtnApply.Visibility = Visibility.Hidden;
                    break;
            }

            LblNameTitle.Text = title;
            TxtError.Text = msg;
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnAccess_Click(object sender, RoutedEventArgs e)
        {
            AddDisk.isDelete = true;
            Close();
        }

        private void BtnNotAccess_Click(object sender, RoutedEventArgs e)
        {
            AddDisk.isDelete = false;
            Close();
        }
    }
}
