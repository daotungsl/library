﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Dictation;
using LibraryV2.Control;
using LibraryV2.Entity;
using LibraryV2.Factory;
using LibraryV2.Factory.Socket;
using LibraryV2.Invoker;
using TN.LibraryV2;
using TN.Shared.DTO;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;
using TN.Shared.Service;
using Button = System.Windows.Controls.Button;
using ComboBox = System.Windows.Controls.ComboBox;
using ListView = System.Windows.Controls.ListView;
using MessageBox = System.Windows.MessageBox;

namespace LibraryV2
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static SyncSocketClient Client;
        public static DeviceItems DataDeviceItems;
        public static Devices DataDevices;
        public static Items DataItems;
        public static bool ConnectClient = false;
        public static int SkipRecord = 0;
        public static int NextRecord = 10;
        public static int DefaultRecord = 10;
        public static int LastRecord = 0;
        public static int TotalRecord = 1;
        public static int? CountListItem = 0;
        public static int? PageActive = 0;
        List<ListItemShow> listItems = new List<ListItemShow>();
        static modbus modbus = new modbus();


        private DispatcherTimer dispatcherTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 10), IsEnabled = true };

        #region [Handle Btn Checked]
        public static bool RecordChecked = false;
        public static bool CheckAll = false;
        public static bool CheckDiskName = false;
        public static bool CheckSingle = false;
        public static bool CheckComposer = false;
        public static bool CheckGroup = false;

        private void SetDefault()
        {
            CheckAll = false;
            CheckDiskName = false;
            CheckSingle = false;
            CheckComposer = false;
            CheckGroup = false;
        }
        #endregion

        #region [Handle Getdata]

        private bool _isGetAll = true;
        private bool _isGetLike = false;
        private bool _isGetHistory = false;

        private bool _isGetSearchAll = true;
        private bool _isGetSearchDiskName = false;
        private bool _isGetSearchSinger = false;
        private bool _isGetSearchComposer = false;
        private bool _isGetSearchGroup = false;

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            //Hidden Item
            TxtMask.Visibility = Visibility.Hidden;
            
            GenerateBox();
            //ShowWaitForm();
            WindowMain.Opacity = 0;
            LoadForm();

            dispatcherTimer.Tick += new EventHandler(ResponsibleMainTimer_Tick);


        }




        private void ResponsibleMainTimer_Tick(object sender, EventArgs e)
        {
            ResponsibleMain();
            if (WindowMain.Opacity != 1)
            {
                WindowMain.Opacity += 0.04;
            }


        }

        private void ShowWaitForm()
        {
            var loadingForm = new LoadingInit();
            loadingForm.ShowDialog();
        }

        private void ResponsibleMain()
        {
            var withWindowMain = WindowMain.Width;
            //var heightWindowMain = WindowMain.Height;

            var heightWindowMain = (withWindowMain / 16) * 9;


            var withMain = Main.Width;
            var heightMain = Main.Height;

            var scaleX = withWindowMain / withMain;
            var scaleY = heightWindowMain / heightMain;




            Main.LayoutTransform = new ScaleTransform(scaleX, scaleY);
            Main.UpdateLayout();
            //WindowMain.Opacity = 1;

            //Main.LayoutTransform = new ScaleTransform(1.5, 1.5);
            //Main.UpdateLayout();
            //dispatcherTimer.Interval = new TimeSpan(10, 0, 0, 0, 0);
        }

        public class ListItemShow
        {
            public int Id { get; set; }
            public int Like { get; set; }
            public string Banner { get; set; }
            public string DiskName { get; set; }

            public string SingerComposer { get; set; }

            public string BoxTray { get; set; }
            public string GroupItem { get; set; }
            public string Description { get; set; }
        }




        private void GenerateBox()
        {
            List<Devices> listDevices = new List<Devices>();
            if (CsvDTO.ListDevices?.Count == 0 || CsvDTO.ListDevices == null)
            {
                for (int i = 1; i < (AppSetting.BoxColumn * AppSetting.BoxRow + 1); i++)
                {
                    var data = new Devices
                    {
                        Name = "Ngăn " + i,
                        Address = $"Vị trí thứ {i} trog dãy",
                        Status = 1,
                        COM = "3",
                        Description = "",
                        IMEI = "000" + i,
                        IP = $"{i}",
                        PORT = 81,
                        Id = i
                    };
                    listDevices.Add(data);
                }
                _ = CsvDTO.Write(listDevices);
            }
            List<DeviceItems> listDeviceItems = new List<DeviceItems>();
            if (CsvDTO.ListDeviceItems?.Count == 0 || CsvDTO.ListDeviceItems == null)
            {
                for (int i = 1; i < (AppSetting.BoxColumn * AppSetting.BoxRow + 1); i++)
                {
                    for (int j = 1; j < 11; j++)
                    {
                        var data = new DeviceItems
                        {
                            Status = 0,
                            DeviceId = i,
                            Order = j,
                            Id = i * 10 + j - 10
                        };
                        listDeviceItems.Add(data);
                    }

                }
                _ = CsvDTO.Write(listDeviceItems);
            }
        }


        private async void LoadForm()
        {
            //Thread thread = new Thread(InitSocketClient);
            //thread.Start();
            InitSocketClient();


            //Hidden item
            ImageLoading.Visibility = Visibility.Hidden;
            BtnPrev.Visibility = Visibility.Hidden;
            LblPage.Visibility = Visibility.Hidden;
            BodEmptyItem.Visibility = Visibility.Hidden;
            BtnEditHidden.Visibility = Visibility.Hidden;

            //BtnNext.Visibility = Visibility.Hidden;
            await CsvDTO.ReadAllAsync();

            GetData();
            ResponsibleMain();

            ChangeButtonSearchCheck(BtnCheckAll);
            ChangeButtonListViewCheck(BtnListAll);
        }



        #region [Socket Client]
        //Init Socket
        private void InitSocketClient()
        {
            byte[] healthCheckBytes = Encoding.UTF8.GetBytes(AppSetting.HealthCheck);
            Client = new SyncSocketClient(AppSetting.SocketClientIP, AppSetting.SocketClientPort, AppSetting.SocketConnectionTimeout, true, false, AppSetting.ClientHealthCheckInterval, healthCheckBytes, "");
            Client.DataReceived += new SyncSocketClient.DataReceivedHandler(SocketListener_DataReceived);
            ConnectClient = Client.Start();

        }
        private void SocketListener_DataReceived(SyncSocketClient client, string data)
        {
            //MessageBox.Show($"Client Receive: {data}");
            if (this.Dispatcher.CheckAccess())
            {
                if (data.StartsWith("COM"))
                {
                    MethodCheckCom(data);
                }
                else
                {
                    this.MethodOpenBox(data);
                }

            }
            else
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() =>
                    {
                        if (data.StartsWith("COM"))
                        {
                            MethodCheckCom(data);
                        }
                        else
                        {
                            this.MethodOpenBox(data);
                        }
                    }));
            }
        }

        #endregion

        #region [Get Data]

        private void GetData()
        {
            //ListItems.Opacity = 0.5;
            //Thread.Sleep(400);
            ThreadGetData();
            //Thread thread = new Thread(ThreadGetData);
            //thread.Start();



        }

        private void ThreadGetData()
        {
            try
            {
                listItems.Clear();

                ImageLoading.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() => { ImageLoading.Visibility = Visibility.Visible; }));
                string txtSearch = "";
                if (TxtSearch.Text == "" || TxtSearch.Text == "Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ...")
                {
                    _isGetSearchAll = false;
                }
                else
                {
                    txtSearch = TxtSearch.Text;
                }

                if (TxtSearch.Text.ToUpper() == AppSetting.HiddenCode.ToUpper())
                {
                    BtnEditHidden.Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action(() => { BtnEditHidden.Visibility = Visibility.Visible; }));
                }
                else
                {
                    BtnEditHidden.Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action(() => { BtnEditHidden.Visibility = Visibility.Hidden; }));
                }
                //var items = ItemsControllerCsv.GetAllWithInfo(SkipRecord, NextRecord);
                var items = ItemsControllerCsv.FullTextSearch(SkipRecord, NextRecord, txtSearch, _isGetAll, _isGetLike, _isGetHistory, _isGetSearchAll, _isGetSearchDiskName, _isGetSearchSinger, _isGetSearchComposer, _isGetSearchGroup);

                if (items.Item1 == null || items.Item2 == null)
                {
                    items = new Tuple<List<ItemsModel>, int?>(null, 0);
                }
                else
                {
                    foreach (var itemModel in items.Item1)
                    {
                        var item = new ListItemShow();
                        item.Id = itemModel.Id;
                        item.DiskName = $"{FirstCharToUpper(itemModel.Name)}";
                        item.Banner = itemModel.Banner == "" ? "disk-default2.png" : Directory.GetCurrentDirectory() + itemModel.Banner;
                        item.BoxTray = $"{FirstCharToUpper(itemModel.DeviceName)} / {FirstCharToUpper(itemModel.DeviceItemName)}";
                        item.SingerComposer = $"{itemModel.GroupSingerName ?? ". . . . . . . . . ."} / {itemModel.GroupComposedName ?? ". . . . . . . . . ."}";
                        item.GroupItem = itemModel.GroupItemName;
                        item.Description = FactoryFunction.Base64Decode(itemModel.Description);
                        item.Like = itemModel.Like;
                        listItems.Add(item);
                        //listItems.Add(new ListItemShow() { DiskName = "ĐĨA NHỰA NHẠC XƯA HƯƠNG LAN VỚI 4 CA KHÚC", SingerComposer = "Hương Lan/Phạm Trọng", BoxTray = "Ngăn 1 / 1" });
                    }
                }



                //listItems.Add(new ListItemShow() { DiskName = "ĐĨA NHỰA NHẠC XƯA HƯƠNG LAN VỚI 4 CA KHÚC", SingerComposer = "Hương Lan/Phạm Trọng", BoxTray = "Ngăn 1 / 1" });
                //listItems.Add(new ListItemShow() { DiskName = "ĐĨA LP VINYL TIẾNG HÁT TỪ MẶT TRỜI VÀ ÁNH LỬA ĐĨA LP VINYL TIẾNG HÁT TỪ", SingerComposer = "Tiến Bắc/Phan Sơn", BoxTray = "Ngăn 1 / 2" });
                //listItems.Add(new ListItemShow() { DiskName = "ĐĨA THAN VIỆT NAM MĐRAK MĐRAK", SingerComposer = ".../...", BoxTray = "Ngăn 1 / 1" });



                ListItems.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() =>
                    {
                        ListItems.ItemsSource = null;
                        ListItems.ItemsSource = listItems;

                    }));


                if (CountListItem != items.Item2)
                {
                    CountListItem = items.Item2;
                    ControlBtnPaginate(items.Item2);
                }

                BtnPrev.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() =>
                    {
                        if (SkipRecord == 0)
                        {
                            BtnPrev.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            BtnPrev.Visibility = Visibility.Visible;
                        }
                    }));

                LblTotalDisk.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() => { LblTotalDisk.Content = items.Item2; }));

                BodEmptyItem.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() =>
                    {
                        if (items.Item2 > 0)
                        {
                            BodEmptyItem.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            BodEmptyItem.Visibility = Visibility.Visible;

                        }
                    }));
                ImageLoading.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() => { ImageLoading.Visibility = Visibility.Hidden; }));
                BtnChoosed();
                //ListIteams.ItemsSource = listItems;
                //ImageLoading.Visibility = Visibility.Hidden;
            }
            catch (Exception e)
            {


            }
        }

        private void ControlBtnPaginate(int? totalItem)
        {

            var page = totalItem / NextRecord;
            page = totalItem % NextRecord != 0 ? page + 1 : page;

            if (page == 1)
            {
                SetInvoker.Button(BtnFistPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnMid, TypeAction.Content, 0);
                SetInvoker.Button(BtnLastPage, TypeAction.Content, 1);

                //BtnChoosed(BtnLastPage);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnMid, TypeAction.Visible, false);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, false);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
                SetInvoker.Button(BtnPrev, TypeAction.Margin, new Thickness(BtnMid.Margin.Left, BtnMid.Margin.Top, BtnMid.Margin.Right, BtnMid.Margin.Bottom));

            }
            else if (page == 2)
            {

                SetInvoker.Button(BtnFistPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnMid, TypeAction.Content, 1);
                SetInvoker.Button(BtnLastPage, TypeAction.Content, 2);

                //BtnChoosed(BtnMid);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnMid, TypeAction.Visible, true);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, true);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
                SetInvoker.Button(BtnPrev, TypeAction.Margin, new Thickness(BtnThirdPage.Margin.Left, BtnThirdPage.Margin.Top, BtnThirdPage.Margin.Right, BtnThirdPage.Margin.Bottom));

            }
            else if (page == 3)
            {

                SetInvoker.Button(BtnFistPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 1);
                SetInvoker.Button(BtnMid, TypeAction.Content, 2);
                SetInvoker.Button(BtnLastPage, TypeAction.Content, 3);

                //BtnChoosed(BtnThirdPage);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnMid, TypeAction.Visible, true);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, true);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
                SetInvoker.Button(BtnPrev, TypeAction.Margin, new Thickness(BtnSecondPage.Margin.Left, BtnSecondPage.Margin.Top, BtnSecondPage.Margin.Right, BtnSecondPage.Margin.Bottom));

            }
            else if (page == 4)
            {

                SetInvoker.Button(BtnFistPage, TypeAction.Content, 0);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 1);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 2);
                SetInvoker.Button(BtnMid, TypeAction.Content, 3);
                SetInvoker.Button(BtnLastPage, TypeAction.Content, 4);

                //BtnChoosed(BtnSecondPage);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, false);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnMid, TypeAction.Visible, true);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, true);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
                SetInvoker.Button(BtnPrev, TypeAction.Margin, new Thickness(BtnFistPage.Margin.Left, BtnFistPage.Margin.Top, BtnFistPage.Margin.Right, BtnFistPage.Margin.Bottom));

            }
            else if (page == 5)
            {

                SetInvoker.Button(BtnFistPage, TypeAction.Content, 1);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 2);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 3);
                SetInvoker.Button(BtnMid, TypeAction.Content, 4);
                SetInvoker.Button(BtnLastPage, TypeAction.Content, 5);

                //BtnChoosed(BtnFistPage);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnMid, TypeAction.Visible, true);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, true);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
            }
            else if (page > 5)
            {

                SetInvoker.Button(BtnFistPage, TypeAction.Content, 1);
                SetInvoker.Button(BtnSecondPage, TypeAction.Content, 2);
                SetInvoker.Button(BtnThirdPage, TypeAction.Content, 3);
                SetInvoker.Button(BtnMid, TypeAction.Content, "...");
                SetInvoker.Button(BtnLastPage, TypeAction.Content, page);

                //BtnChoosed(BtnFistPage);

                SetInvoker.Button(BtnFistPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnSecondPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnThirdPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnMid, TypeAction.Visible, true);
                SetInvoker.Button(BtnLastPage, TypeAction.Visible, true);
                SetInvoker.Button(BtnNext, TypeAction.Visible, true);
                SetInvoker.Button(BtnPrev, TypeAction.Visible, false);
            }

        }

        private void BtnChoosed()
        {

            //SetInvoker.Button(button, TypeAction.Color, Color.FromRgb(32, 164, 255));
            //var listBtnPaginate = new List<Button> { BtnFistPage, BtnSecondPage, BtnThirdPage, BtnMid, BtnLastPage };
            //foreach (var e in listBtnPaginate)
            //{
            //    if (button != e)
            //    {
            //        SetInvoker.Button(e, TypeAction.Color, Color.FromRgb(54, 62, 81));
            //    }
            //}


            try
            {
                var listBtnPaginate = new List<Button> { BtnFistPage, BtnSecondPage, BtnThirdPage, BtnMid, BtnLastPage };
                foreach (var e in listBtnPaginate)
                {
                    e.Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action(() =>
                        {
                            if (e.Content != "...")
                            {
                                if (Convert.ToInt32(e.Content) == (SkipRecord / 10) + 1)
                                {
                                    SetInvoker.Button(e, TypeAction.Color, Color.FromRgb(32, 164, 255));
                                    PageActive = Convert.ToInt32(e.Content);
                                }
                                else
                                {
                                    SetInvoker.Button(e, TypeAction.Color, Color.FromRgb(54, 62, 81));

                                }
                            }


                        }));

                }

            }
            catch (Exception e)
            {

            }

        }



        #endregion

        public static string FirstCharToUpper(string input)
        {
            switch (input)
            {
                case null:
                    throw new ArgumentNullException(nameof(input));
                case "":
                    throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default:
                    input = input.ToLower();
                    return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }
        private void record_Click(object sender, RoutedEventArgs e)
        {
            if (!RecordChecked)
            {
                TxtMask.Visibility = Visibility.Visible;

                ImageMic.Source = new BitmapImage(new Uri("Assets/mic-rec.png", UriKind.Relative));
                //InfiniteStreaming.isRun = true;
                TxtSearch.Text = "Vui lòng nói thông tin muốn tìm...";
                RecordChecked = true;
                SpeechToText();

            }
            else
            {
                if (TxtSearch.Text == "Vui lòng nói thông tin muốn tìm...")
                {
                    TxtSearch.Text = "Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ...";
                }
                TxtMask.Visibility = Visibility.Hidden;
                ImageMic.Source = new BitmapImage(new Uri("Assets/mic-normal.png", UriKind.Relative));
                RecordChecked = false;
                //InfiniteStreaming.isRun = false;

            }
        }
        private void SpeechToText()
        {
            //WorkThreadFunction();
            Thread thread = new Thread(WorkThreadFunction);
            thread.Start();
        }

        public static bool status = false;

        public async void WorkThreadFunction()
        {

            try
            {
                InfiniteStreaming.Log = OutputString;
                int x = await InfiniteStreaming.RecognizeAsync();
            }
            catch (Exception ex)
            {
                // log errors
            }
        }
        public void OutputString(string s)
        {

            Console.WriteLine("Text Here: " + s);


            if (TxtSearch.Dispatcher.CheckAccess())
            {
                if (RecordChecked)
                {
                    TxtSearch.Text = s.ToUpper();
                    SkipRecord = 0;
                    GetData();
                    record_Click(null, null);

                    TxtMask.Visibility = Visibility.Hidden;
                }
               
            }
            else
            {
                TxtSearch.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() =>
                    {
                        OutputString(s);

                    }));
            }
            //txtSearch.Dispatcher.Invoke(DispatcherPriority.Normal,
            //    new Action(() => { OutputString(s);}));
            //txtSearch.Text = s;

            //txtChoice.EditValue = s;
            //    GetDataByBtn(_SkipRecord, _NextRecord);
            //    btnCRecord.Checked = false;




        }

        private void ChangeButtonSearchCheck(Button active)
        {

            var listBtnCheck = new List<Button> { BtnCheckAll, BtnCheckDiskName, BtnCheckSingle, BtnCheckComposer, BtnCheckGroup };
            foreach (var button in listBtnCheck)
            {
                if (button != active)
                {
                    button.Background = new SolidColorBrush(Color.FromRgb(83, 101, 137));
                    button.Foreground = new SolidColorBrush(Color.FromRgb(179, 187, 202));
                    SelectButtonSearchCheck(button, false);

                }
                else
                {
                    active.Background = new SolidColorBrush(Color.FromRgb(32, 164, 255));
                    active.Foreground = Brushes.White;
                    SelectButtonSearchCheck(button, true);
                }
            }

        }

        private void SelectButtonSearchCheck(Button button, bool isSelect)
        {
            switch (button.Name)
            {
                case "BtnCheckAll":
                    _isGetSearchAll = isSelect; break;
                case "BtnCheckDiskName":
                    _isGetSearchDiskName = isSelect; break;
                case "BtnCheckSingle":
                    _isGetSearchSinger = isSelect; break;
                case "BtnCheckComposer":
                    _isGetSearchComposer = isSelect; break;
                case "BtnCheckGroup":
                    _isGetSearchGroup = isSelect; break;
            }
        }
        private void btnCheckAll_Click(object sender, RoutedEventArgs e)
        {
            ChangeButtonSearchCheck(BtnCheckAll);
            SetDefault();
            CheckAll = true;
        }

        private void btnCheckDiskName_Click(object sender, RoutedEventArgs e)
        {
            ChangeButtonSearchCheck(BtnCheckDiskName);
            SetDefault();
            CheckDiskName = true;

        }

        private void btnCheckSingle_Click(object sender, RoutedEventArgs e)
        {
            ChangeButtonSearchCheck(BtnCheckSingle);
            SetDefault();
            CheckSingle = true;

        }

        private void btnCheckComposer_Click(object sender, RoutedEventArgs e)
        {
            ChangeButtonSearchCheck(BtnCheckComposer);
            SetDefault();
            CheckComposer = true;

        }

        private void btnCheckGroup_Click(object sender, RoutedEventArgs e)
        {
            ChangeButtonSearchCheck(BtnCheckGroup);
            SetDefault();
            CheckGroup = true;

        }

        #region [Add Text From Button To Search]

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 1;
            GetData();

        }
        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 2;
            GetData();

        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 3;
            GetData();

        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 4;
            GetData();

        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 5;
            GetData();

        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 6;
            GetData();

        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 7;
            GetData();

        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 8;
            GetData();

        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 9;
            GetData();

        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += 0;
            GetData();

        }

        private void BtnQ_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "Q";
            GetData();

        }

        private void BtnW_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "W";
            GetData();

        }

        private void BtnE_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "E";
            GetData();

        }

        private void BtnR_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "R";
            GetData();

        }

        private void BtnT_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "T";
            GetData();

        }

        private void BtnY_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "Y";
            GetData();

        }

        private void BtnU_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "U";
            GetData();

        }

        private void BtnI_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "I";
            GetData();

        }

        private void BtnO_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "O";
            GetData();

        }

        private void BtnP_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "P";
            GetData();

        }

        private void BtnA_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "A";
            GetData();

        }

        private void BtnS_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "S";
            GetData();

        }

        private void BtnD_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "D";
            GetData();

        }

        private void BtnF_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();

            TxtSearch.Text += "F";
            GetData();

        }

        private void BtnG_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();

            TxtSearch.Text += "G";
            GetData();

        }

        private void BtnH_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "H";

            GetData();

        }

        private void BtnJ_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "J";

            GetData();

        }

        private void BtnK_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "K";

            GetData();

        }

        private void BtnL_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "L";

            GetData();

        }

        private void BtnZ_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "Z";

            GetData();

        }

        private void BtnX_Click(object sender, RoutedEventArgs e)
        {

            CheckHind();
            TxtSearch.Text += "X";

            GetData();

        }

        private void BtnC_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "C";

            GetData();

        }

        private void BtnV_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "V";

            GetData();

        }

        private void BtnB_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "B";

            GetData();

        }

        private void BtnN_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "N";

            GetData();

        }

        private void BtnM_Click(object sender, RoutedEventArgs e)
        {
            CheckHind();
            TxtSearch.Text += "M";
            GetData();

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string txt = TxtSearch.Text;
                if (txt.Length > 0)
                {
                    TxtSearch.Text = txt.Remove(txt.Length - 1);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }
            CheckHind();
            GetData();

        }

        private void BtnOSK_Click(object sender, RoutedEventArgs e)
        {
            TxtSearch.Text = "Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ...";
            GetData();
        }

        private void CheckHind()
        {
            if (TxtSearch.Text.Equals("Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ..."))
            {
                TxtSearch.Text = "";
                TxtSearch.Foreground = Brushes.Black;
            }
            else if (TxtSearch.Text == "")
            {
                TxtSearch.Text = "Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ...";
                TxtSearch.Foreground = Brushes.DimGray;
            }
        }

        #endregion

        #region [Btn Add]

        private void HandleBtnAdd(TypeBtnAdd type)
        {
            Main.Opacity = 0.5;
            if (type == TypeBtnAdd.Disk)
            {
                var addDisk = new AddDisk(0);
                addDisk.ShowDialog();
            }
            else
            {
                var addSingle = new AddSingle(type);
                addSingle.ShowDialog();
            }
            GetData();
            Main.Opacity = 1;
        }
        private void BtnAddDisk_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnAdd(TypeBtnAdd.Disk);
        }
        private void BtnAddSinger_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnAdd(TypeBtnAdd.Singer);

        }

        private void BtnComposer_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnAdd(TypeBtnAdd.Composer);

        }

        private void BtnGroup_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnAdd(TypeBtnAdd.Group);

        }




        #endregion

        #region Btn Edit]
        private void HandleEdit(TypeBtnEdit type, ListItemShow data)
        {
            Main.Opacity = 0.5;
            if (type == TypeBtnEdit.Disk)
            {
                var addDisk = new AddDisk(data.Id);
                addDisk.ShowDialog();
            }
            //else
            //{
            //    var addSingle = new AddSingle(type);
            //    addSingle.ShowDialog();
            //}
            GetData();
            Main.Opacity = 1;
        }

        #endregion

        #region [Handle Paginate]
        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            SkipRecord += NextRecord;


            GetData();
            MethodNextpage(false);
            BtnChoosed();

        }

        private void MethodNextpage(bool isClick)
        {
            if (Convert.ToInt32(BtnThirdPage.Content) == (SkipRecord / 10) + (isClick ? 1 : 2))
            {


                if (BtnMid.Content == "...")
                {
                    if (Convert.ToInt32(BtnThirdPage.Content) + 3 == Convert.ToInt32(BtnLastPage.Content))
                    {
                        BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) + 1;
                        BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) + 1;
                        BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) + 1;
                        SetInvoker.Button(BtnMid, TypeAction.Content, Convert.ToInt32(BtnThirdPage.Content) + 1);
                    }
                    else
                    {
                        BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) + 2;
                        BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) + 2;
                        BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) + 2;
                    }

                }
                if (Convert.ToInt32(BtnThirdPage.Content) > Convert.ToInt32(BtnLastPage.Content) - 3)
                {
                    SetInvoker.Button(BtnMid, TypeAction.Content, Convert.ToInt32(BtnThirdPage.Content) + 1);
                }

            }
            if (Convert.ToInt32(BtnLastPage.Content) == (SkipRecord / 10) + 1)
            {
                BtnNext.Visibility = Visibility.Hidden;
            }
        }
        private void BtnPrev_Click(object sender, RoutedEventArgs e)
        {
            SkipRecord -= NextRecord;

            GetData();
            MethodPrevpage(false);
            BtnChoosed();

        }

        private void MethodPrevpage(bool isClick)
        {


            if (Convert.ToInt32(BtnFistPage.Content) == (SkipRecord / 10) + (isClick ? 1 : 2))
            {
                if (Convert.ToInt32(BtnFistPage.Content) != 1)
                {
                    if (Convert.ToInt32(BtnThirdPage.Content) > Convert.ToInt32(BtnLastPage.Content) - 3)
                    {
                        SetInvoker.Button(BtnMid, TypeAction.Content, "...");
                    }

                    if (Convert.ToInt32(BtnSecondPage.Content) == 3 || Convert.ToInt32(BtnFistPage.Content) == 2)
                    {
                        BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) - 1;
                        BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) - 1;
                        BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) - 1;
                    }
                    else
                    {
                        BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) - 2;
                        BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) - 2;
                        BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) - 2;
                    }

                    if (Convert.ToInt32(BtnFistPage.Content) == Convert.ToInt32(BtnLastPage.Content) - 1)
                    {
                        SetInvoker.Button(BtnMid, TypeAction.Content, Convert.ToInt32(BtnThirdPage.Content) + 1);
                    }


                }
                else
                {
                    BtnPrev.Visibility = Visibility.Hidden;
                }

            }
            if (Convert.ToInt32(BtnLastPage.Content) != (SkipRecord / 10) + 1)
            {
                BtnNext.Visibility = Visibility.Visible;
            }
        }
        private void BtnFistPage_Click(object sender, RoutedEventArgs e)
        {
            var txtBtn = Convert.ToInt32(BtnFistPage.Content) - 1;
            if (txtBtn != 0)
            {
                if ((SkipRecord / 10) < txtBtn)
                {
                    SkipRecord = txtBtn * 10;
                    MethodNextpage(true);
                }
                else
                {
                    SkipRecord = txtBtn * 10;
                    MethodPrevpage(true);
                }
                //BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) - 2;
                //BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) - 2;
                //BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) - 2;
            }
            else
            {
                SkipRecord = txtBtn * 10;
                BtnNext.Visibility = Visibility.Visible;
            }


            GetData();

        }

        private void BtnSecondPage_Click(object sender, RoutedEventArgs e)
        {
            var txtBtn = Convert.ToInt32(BtnSecondPage.Content) - 1;
            SkipRecord = txtBtn * 10;

            GetData();

        }


        private void BtnThirdPage_Click(object sender, RoutedEventArgs e)
        {
            var txtBtn = Convert.ToInt32(BtnThirdPage.Content) - 1;
            if ((SkipRecord / 10) < txtBtn)
            {
                SkipRecord = txtBtn * 10;
                MethodNextpage(true);
            }
            else
            {
                SkipRecord = txtBtn * 10;
                MethodPrevpage(true);
            }



            //BtnFistPage.Content = Convert.ToInt32(BtnFistPage.Content) + 2;
            //BtnSecondPage.Content = Convert.ToInt32(BtnSecondPage.Content) + 2;
            //BtnThirdPage.Content = Convert.ToInt32(BtnThirdPage.Content) + 2;


            GetData();

        }
        private void BtnMid_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var txtBtn = Convert.ToInt32(BtnMid.Content) - 1;
                SkipRecord = txtBtn * 10;

                GetData();
            }
            catch
            {

            }

        }
        private void BtnLastPage_Click(object sender, RoutedEventArgs e)
        {
            var txtBtn = Convert.ToInt32(BtnLastPage.Content) - 1;
            if ((SkipRecord / 10) < txtBtn)
            {
                SkipRecord = txtBtn * 10;
                MethodNextpage(true);
            }
            else
            {
                SkipRecord = txtBtn * 10;
                MethodPrevpage(true);
            }

            GetData();

        }


        #endregion

        #region [Handle ListView]
        private void BtnHistory_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            ChangeButtonListViewCheck(button);
            SkipRecord = 0;
            GetData();
        }
        private void BtnListAll_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            ChangeButtonListViewCheck(button);
            SkipRecord = 0;
            GetData();
        }

        private void BtnListFavourite_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            ChangeButtonListViewCheck(button);
            SkipRecord = 0;
            GetData();
        }
        private void ChangeButtonListViewCheck(Button active)
        {

            var listBtnCheck = new List<Button> { BtnListAll, BtnListFavourite, BtnListHistory };
            foreach (var button in listBtnCheck)
            {
                if (button != active)
                {
                    button.Background = new SolidColorBrush(Color.FromRgb(32, 37, 48));
                    button.Foreground = new SolidColorBrush(Color.FromRgb(179, 187, 202));
                    SelectButtonListViewCheck(button, false);

                }
                else
                {
                    active.Background = new SolidColorBrush(Color.FromRgb(32, 164, 255));
                    active.Foreground = Brushes.White;
                    SelectButtonListViewCheck(button, true);

                }
            }

        }
        private void SelectButtonListViewCheck(Button button, bool isSelect)
        {
            switch (button.Name)
            {
                case "BtnListAll":
                    _isGetAll = isSelect; break;
                case "BtnListFavourite":
                    _isGetLike = isSelect; break;
                case "BtnListHistory":
                    _isGetHistory = isSelect; break;

            }
        }
        private void BtnFavourite_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button != null)
            {

                Image image = button.Content as Image;
                if (image.Source.ToString().Contains("out-heart.png"))
                {
                    image.Source = new BitmapImage(new Uri("Assets/in-heart.png", UriKind.Relative));
                    image.Opacity = 1;
                }
                else
                {
                    image.Source = new BitmapImage(new Uri("Assets/out-heart.png", UriKind.Relative));
                    image.Opacity = 0.4;

                }

                var listItemShow = (ListItemShow)button.CommandParameter;
                //MessageBox.Show($"{listItemShow.DiskName} - {listItemShow.SingerComposer} - {listItemShow.BoxTray}");
                var item = CsvDTO.ListItems.First(x => x.Id == listItemShow.Id);
                item.Like = item.Like == 0 ? 1 : 0;
                ItemsControllerCsv.Edit(item);
            }

        }

        private void OpenTray(object sender, RoutedEventArgs e)
        {
            Main.Opacity = 0.5;
            Button button = sender as Button;
            if (button != null)
            {
                var listItemShow = (ListItemShow)button.CommandParameter;
                DataItems = CsvDTO.ListItems.First(x => x.Id == listItemShow.Id);
                DataDevices = DevicesControllerCsv.GetById(DataItems.DeviceId);
                DataDeviceItems = DeviceItemsControllerCsv.GetById(DataItems.DeviceItemId);

                var dataDeviceIp = Convert.ToInt32(DataDevices.IP) > 10 ? Convert.ToInt32(DataDevices.IP) % 10 : Convert.ToInt32(DataDevices.IP);

                int OrderMode = DataDeviceItems.Order;
                if (!AppSetting.IsDirectionTray)
                {
                    OrderMode = MapTray(DataDeviceItems.Order);
                }

                string infoItem = $"{DataDevices.COM};{(dataDeviceIp == 0 ? 10 : dataDeviceIp)};{Convert.ToByte(OrderMode)}";

                try
                {
                    if (!Client.Send(infoItem))
                    {
                        MethodOpenBox("using_local");
                    }
                }
                catch
                {
                    try
                    {
                        MethodOpenBox("using_local");
                    }
                    catch (Exception ex)
                    {
                        Utilities.WriteErrorLog("OpenBox", ex.ToString());
                    }
                }


                //var nofi = new OpenSuccess(item.DeviceId,item.DeviceItemId);
                //nofi.ShowDialog();

            }
            Main.Opacity = 1;
            //MessageBox.Show(b.CommandParameter.ToString());

        }

        private int MapTray(int num)
        {
            return num == 1 ? 10 : num == 2 ? 9 : num == 3 ? 8 : num == 4 ? 7 : num == 5 ? 6 : num == 6 ? 5 : num == 7 ? 4 : num == 8 ? 3 : num == 9 ? 2 : 1;

        }
        public static void MethodCheckCom(string MsgReceive)
        {
            var listCom = new List<string>();
            if (MsgReceive == "COM_Empty")
            {

                listCom.Add("Empty Com");
            }
            else
            {
                listCom.Add("Please Select");
                listCom.AddRange(MsgReceive.Split('|').ToList());
            }
            CostGlobal.CbbComBlock1.ItemsSource = listCom;
            foreach (var com in listCom)
            {
                if (CostGlobal.LblComBlock1.Content.Equals(com))
                {
                    CostGlobal.CbbComBlock1.SelectedValue = com;
                }
                else
                {
                    CostGlobal.CbbComBlock1.SelectedIndex = 0;
                }
            }

            CostGlobal.CbbComBlock2.ItemsSource = listCom;

            foreach (var com in listCom)
            {
                if (CostGlobal.LblComBlock2.Content.Equals(com))
                {
                    CostGlobal.CbbComBlock2.SelectedValue = com;
                }
                else
                {
                    CostGlobal.CbbComBlock2.SelectedIndex = 0;
                }
            }

        }
        public void MethodOpenBox(string MsgReceive)
        {

            bool isOpen = true;

            if (!modbus.sp.IsOpen && (MsgReceive == "DK_com_false" || MsgReceive == "using_local"))
            {
                if (MsgReceive == "using_local")
                {
                    isOpen = modbus.Open($"COM{DataDevices.COM}", 9600, 8, System.IO.Ports.Parity.Even, System.IO.Ports.StopBits.One);
                }


                if (!isOpen || (MsgReceive == "DK_com_false"))
                {
                    Console.WriteLine($"Open [COM{DataDevices.COM}] error !!!");

                    var nofi = new Notification(TypeNofi.Error, $"Không thể kết nối tới {DataDevices.Name}." + "\n" +
                                                                $"Hoặc {DataDevices.Name} chưa được cài đặt cấu hình" + "\n" +
                                                                "Vui lòng kiểm tra lại cài đặt cấu hình, nguồn điện hoặc dây kết nối !!!", "LỖI KẾT NỐI");
                    nofi.ShowDialog();
                    //btnOpen.Enabled = true;

                }

            }
            if (modbus.sp.IsOpen || MsgReceive == "DK_opentray_true" || MsgReceive == "DK_opentray_false" || MsgReceive == "DK_openbox_false")
            {
                if (modbus.sp.PortName == $"COM{DataDevices.COM}" || MsgReceive == "DK_opentray_true" || MsgReceive == "DK_opentray_false")
                {
                    bool openSuccess = true;

                    if (MsgReceive == "using_local")
                    {
                        openSuccess = modbus.Openbox(Convert.ToByte(DataDevices.IP.Last().ToString()), Convert.ToByte(DataDeviceItems.Order));
                    }

                    if (openSuccess || MsgReceive == "DK_opentray_true")
                    {

                        //var form = new SuccessOpen();
                        DataItems.LastOpen = DateTime.Now;
                        ItemsControllerCsv.Edit(DataItems);
                        var nofi = new OpenSuccess(Convert.ToInt32(DataDevices.IP), DataDeviceItems.Order);
                        nofi.ShowDialog();
                        Console.WriteLine($"Open: BoxId = {DataDevices.IP}, TrayId = {DataDeviceItems.Order} success !!!");
                        //btnOpen.Enabled = true;
                        //txtChoice.EditValue = null;

                        if (openSuccess)
                        {
                            modbus.sp.Close();
                        }
                    }
                    else
                    {

                        Console.WriteLine($"Open: BoxId = {DataDevices.IP}, TrayId = {DataDeviceItems.Order} error.");

                        var nofi = new Notification(TypeNofi.Error, $"Không thể mở ô số {DataDeviceItems.Order}" + "\n" + $"Tại {DataDevices.Name}." + "\n" +
                                                                    "vui lòng kiểm tra lại thiết bị hoặc dây kết nối !!!", "LỖI KẾT NỐI");
                        nofi.ShowDialog();
                        //btnOpen.Enabled = true;
                        if (isOpen)
                        {
                            modbus.sp.Close();
                        }
                    }
                }
                else
                {
                    Console.WriteLine($"Open [BoxId{DataDevices.IP}] error !!!");

                    var nofi = new Notification(TypeNofi.Error, $"Không thể kết nối tới {DataDevices.Name}." + "\n" +
                                                                $"Hoặc {DataDevices.Name} chưa được cài đặt cấu hình" + "\n" +
                                                                "Vui lòng kiểm tra lại cài đặt cấu hình, nguồn điện hoặc dây kết nối !!!", "LỖI KẾT NỐI");
                    nofi.ShowDialog();
                    //btnOpen.Enabled = true;
                }



            }
        }

        private void ListItems_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = (sender as ListView)?.SelectedItem;
            if (item != null)
            {
                var user = (ListItemShow)item;
                TxtbNameInfo.Text = user.DiskName.ToUpper();
                LblSingerInfo.Content = user.SingerComposer.Split('/')[0];
                LblComposerInfo.Content = user.SingerComposer.Split('/')[1];
                LblGroupInfo.Content = user.GroupItem;
                RtxbDesInfo.Document.Blocks.Clear();
                RtxbDesInfo.Document.Blocks.Add(new Paragraph(new Run($"{user.Description}")));
                if (user.Banner != "Control/no-imagelist.png")
                {
                    try
                    {
                        ImageDetail.ImageSource = new BitmapImage(new Uri(user.Banner));
                    }
                    catch
                    {
                        ImageDetail.ImageSource = new BitmapImage(new Uri($@"{Directory.GetCurrentDirectory()}\Data\Image\disk-default2.png"));
                    }
                }
                else
                {
                    ImageDetail.ImageSource = new BitmapImage(new Uri($@"{Directory.GetCurrentDirectory()}\Data\Image\disk-default2.png"));

                }
            }
        }

        private void ListItems_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = (sender as ListView)?.SelectedItem;
            if (item != null)
            {
                var data = (ListItemShow)item;
                //MessageBox.Show($"In Right click {user.DiskName} - {user.SingerComposer} - {user.BoxTray}");
                HandleEdit(TypeBtnEdit.Disk, data);
            }
        }




        #endregion




        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Environment.Exit(0);
            //Close();
        }

        #region [Config]

        private void BtnEditHiden_Click(object sender, RoutedEventArgs e)
        {
            Main.Opacity = 0.5;
            var newForm = new Configuration();
            newForm.ShowDialog();
            BtnEditHidden.Visibility = Visibility.Hidden;
            TxtSearch.Text = "Tìm kiếm đĩa, ca sĩ, bài hát, thể loại ...";
            Main.Opacity = 1;
            GetData();
        }

        #endregion

    }
}
