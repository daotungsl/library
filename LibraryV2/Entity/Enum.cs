﻿namespace LibraryV2.Entity
{
    public enum TypeBtnAdd
    {
        Disk,
        Singer,
        Composer,
        Group
    }
    public enum TypeBtnEdit
    {
        Disk,
        Singer,
        Composer,
        Group
    }

    public enum TypeNofi
    {
        Success,
        Error,
        Warning,
        Information,
        Confirm
    }
}