﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TN.LibraryV2
{
    public class AppSetting
    {
        Configuration config;
        private string folderName = "LibraryProject";

        public AppSetting()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
        }
        
        public static string SocketServerIP = ConfigurationManager.AppSettings["SocketServerIP"];
        public static int SocketServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["SocketServerPort"]);
        public static string SocketClientIP = ConfigurationManager.AppSettings["SocketClientIP"];
        public static int SocketClientPort = Convert.ToInt32(ConfigurationManager.AppSettings["SocketClientPort"]);
        public static int SocketConnectionTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SocketConnectionTimeout"]);
        public static int CheckConnectionInterval = Convert.ToInt32(ConfigurationManager.AppSettings["CheckConnectionInterval"]);
        public static int DataSyncIntervalSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["DataSyncIntervalSeconds"]);
        public static string HealthCheck = ConfigurationManager.AppSettings["HealthCheck"];
        public static int ClientMillisecondTimeout { get { return Convert.ToInt32(ConfigurationManager.AppSettings["ClientMillisecondTimeout"]); } }
        public static int ClientHealthCheckInterval { get { return Convert.ToInt32(ConfigurationManager.AppSettings["ClientHealthCheckInterval"]); } }

        public static int BoxColumn = Convert.ToInt32(ConfigurationManager.AppSettings["BoxColumn"]);
        public static int BoxRow = Convert.ToInt32(ConfigurationManager.AppSettings["BoxRow"]);
        public static bool IsDirectionBox => Convert.ToInt32(ConfigurationManager.AppSettings["IsDirectionBox"]) != 0;
        public static bool IsDirectionTray => Convert.ToInt32(ConfigurationManager.AppSettings["IsDirectionTray"]) != 0;
        public static string HiddenCode = ConfigurationManager.AppSettings["HiddenCode"];


    }
}
