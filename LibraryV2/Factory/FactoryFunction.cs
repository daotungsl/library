﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace LibraryV2.Factory
{
    public class FactoryFunction
    {
        public static string GetFriendlyName(string name)
        {
            try
            {
                string convertName = convertVNtoEN(name);
                var arr = convertName.Split(' ');
                string flname = "";
                foreach (var item in arr)
                {
                    if (item != "")
                    {
                        flname += item[0];
                    }
                }
                return flname.ToUpper();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            var arr = input.Split(' ');
            for (int i = 0; i < arr.Count(); i++)
            {
                if (arr[i] != "")
                {
                    arr[i] = arr[i].First().ToString().ToUpper() + String.Join("", arr[i].Skip(1));
                }
            }
            return String.Join(" ", arr);
        }
        private static string convertVNtoEN(string s)
        {
            try
            {
                Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = s.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static void SaveDirection(string key, string value)
        {

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            xmlDoc.SelectSingleNode($"//appSettings/add[@key='{key}']").Attributes["value"].Value = value;
            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
