﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace LibraryV2.Invoker
{
    public class SetInvoker
    {
         public static void Button(Button button, TypeAction typeAction, object content)
        {
            try
            {
                switch (typeAction)
                {
                    case TypeAction.Visible:
                        if (button.Dispatcher.CheckAccess())
                        {
                            button.Visibility = (bool)content ? Visibility.Visible : Visibility.Hidden;
                        }
                        else
                        {
                            button.Dispatcher.Invoke(DispatcherPriority.Normal,
                                new Action(() => { button.Visibility = (bool)content ? Visibility.Visible : Visibility.Hidden; }));
                        }
                        break;
                    case TypeAction.Content:
                        if (button.Dispatcher.CheckAccess())
                        {
                            button.Content = content;
                        }
                        else
                        {
                            button.Dispatcher.Invoke(DispatcherPriority.Normal,
                                new Action(() => { button.Content = content; }));
                        }
                        break;
                    case TypeAction.Color:
                        if (button.Dispatcher.CheckAccess())
                        {
                            button.Background = new SolidColorBrush((Color)content);
                        }
                        else
                        {
                            button.Dispatcher.Invoke(DispatcherPriority.Normal,
                                new Action(() => { button.Background = new SolidColorBrush((Color)content); }));
                        }
                        break;
                    case TypeAction.Margin:
                        if (button.Dispatcher.CheckAccess())
                        {
                            button.Margin = (Thickness)content;
                        }
                        else
                        {
                            button.Dispatcher.Invoke(DispatcherPriority.Normal,
                                new Action(() => { button.Margin = (Thickness)content; }));
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

    }

    public enum TypeAction
    {
        Visible,
        Enable,
        Content,
        Color,
        Margin
    }
}