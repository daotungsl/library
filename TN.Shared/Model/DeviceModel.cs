﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Shared.Data.Model
{
   public class DeviceModel : Device
    {
        public int Tray { get; internal set; }
    }
}
