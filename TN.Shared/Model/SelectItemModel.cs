﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Shared.Data.Model
{
   public class SelectItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string COM { get; set; }
        public int Order { get; set; }

    }
}
