﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace TN.Shared.DTO_New_Version.DTO
{
   public class GroupComposedControllerCsv
    {
        public static List<GroupComposed> GetAll()
        {
            
                return CsvDTO.ListGroupComposed.ToList();
            
        }

        public static GroupComposed GetByName(string name)
        {
            return CsvDTO.ListGroupComposed.FirstOrDefault(x => x.Name == name);
        }

        public static GroupComposed GetById(int? id)
        {
                return CsvDTO.ListGroupComposed.FirstOrDefault(x => x.Id == id);
            
        }

        public static bool Insert(GroupComposed model)
        {
           try
           {
               _ = CsvDTO.Write<GroupComposed>(model.ToString());
               return true;
           }
           catch
           {
               return false;
           }
        }

        public static GroupComposed Edit(GroupComposed model)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupComposeds.FirstOrDefault(x => x.Id == model.Id);
            //    if (get == null)
            //    {
            //        return null;
            //    }
            //    get.Name = model.Name;
            //    db.SubmitChanges();
            //    return model;
            //}
            return null;

        }

        public static void Delete(int id)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupComposeds.FirstOrDefault(x => x.Id == id);
            //    if (get != null)
            //    {
            //        db.GroupComposeds.DeleteOnSubmit(get);
            //        db.SubmitChanges();
            //    }
            //}
        }

       
    }
}
