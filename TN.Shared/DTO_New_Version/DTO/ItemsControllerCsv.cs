﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.Data.Model;
using TN.Shared.DTO;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;
using GroupComposed = TN.Shared.Data.GroupComposed;
using GroupSinger = TN.Shared.Data.GroupSinger;

namespace TN.Shared.DTO_New_Version.DTO
{

    public class ItemsControllerCsv
    {

        public static List<Items> GetAll()
        {
            return CsvDTO.ListItems?.Count > 0 ? CsvDTO.ListItems : null;
        }

        public static Items GetByItemId(int? id)
        {
            return CsvDTO.ListItems?.FirstOrDefault(x => x.Id == id);
        }

        public static Tuple<List<ItemsModel>, int?> GetAllWithInfo(int _Skip, int _Next)
        {
            try
            {

                var qr = GetAll()?.Select(item =>
                {
                    var model = new ItemsModel();
                    model.Id = item.Id;
                    model.Banner = item.Banner;
                    model.Description = item.Description;
                    model.Name = item.Name;
                    model.Note = item.Note;
                    model.DeviceName = DevicesControllerCsv.GetById(item.DeviceId).Name;
                    model.DeviceItemName = DeviceItemsControllerCsv.GetById(item.DeviceItemId).Order.ToString();
                    model.GroupItemName = GroupItemsControllerCsv.GetById(item.GroupItemId).Name;
                    model.GroupSingerName = GroupSingerControllerCsv.GetById(item.GroupSingerId).Name;
                    model.GroupComposedName = GroupComposedControllerCsv.GetById(item.GroupComposedId).Name;
                    model.Like = item.Like;
                    model.Image = item.Banner;
                    return model;
                }).AsQueryable();
                var list = qr?.Skip(_Skip).Take(_Next).ToList();
                return Tuple.Create(list, qr?.Count());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
       
      
        public static Tuple<List<ItemsModel>, int?> FullTextSearch(
            int skip, int next,
            string text,
            bool isListAll,
            bool isListLike,
            bool isListHistory,
            bool isFullSearch,
            //bool isFriendlySearch,
            bool isDiskName,
            bool isSinger,
            bool isComposer,
            bool isGroupItem

            )
        {

            try
            {
                var qr = new List<Items>();
                if (isListAll)
                {
                    qr = GetAll();
                }
                else if (isListLike)
                {
                    qr = GetAll().Where(x => x.Like == 1).ToList();
                }
                else if (isListHistory)
                {
                    qr = GetAll().OrderByDescending(x => x.LastOpen).ToList();
                }
                if (isFullSearch)
                {
                    var listGroupSinger = CsvDTO.ListGroupSinger.Where(x => x.Name.ToUpper().Contains(text)).ToList();
                    var listGroupItems = CsvDTO.ListGroupItems.Where(x => x.Name.ToUpper().Contains(text)).ToList();
                    var listGroupComposed = CsvDTO.ListGroupComposed.Where(x => x.Name.ToUpper().Contains(text)).ToList();
                    qr = qr.Where(x =>
                        x.Banner.ToUpper().Contains(text) ||
                        x.Description.ToUpper().Contains(text) ||
                        x.Note.ToUpper().Contains(text) ||
                        x.Name.ToUpper().Contains(text) ||
                        listGroupSinger.Any(m => m.Id == x.GroupSingerId) ||
                        listGroupItems.Any(m => m.Id == x.GroupItemId) ||
                        listGroupComposed.Any(m => m.Id == x.GroupComposedId)
                    ).ToList();
                }
                else if (isDiskName)
                {
                    qr = qr.Where(x => x.FriendlyName.StartsWith(text)).ToList();
                }
                else if (isSinger)
                {
                    qr = qr.Where(x => CsvDTO.ListGroupSinger.Any(m => m.Id == x.GroupSingerId && m.FriendlyName.Contains(text))).ToList();
                }
                else if (isComposer)
                {
                    qr = qr.Where(x => CsvDTO.ListGroupComposed.Any(m => m.Id == x.GroupComposedId && m.FriendlyName.Contains(text))).ToList();
                }
                else if (isGroupItem)
                {
                    qr = qr.Where(x => CsvDTO.ListGroupItems.Any(m => m.Id == x.GroupItemId && m.FriendlyName.Contains(text))).ToList();
                }

                var result = qr.Select(item => new ItemsModel
                {
                    Id = item.Id,
                    Banner = item.Banner,
                    Description = item.Description,
                    Name = item.Name,
                    Note = item.Note,
                    DeviceName = DevicesControllerCsv.GetById(item.DeviceId).Name,
                    DeviceItemName = DeviceItemsControllerCsv.GetById(item.DeviceItemId).Order.ToString(),
                    GroupItemName = GroupItemsControllerCsv.GetById(item.GroupItemId).Name,
                    GroupSingerName = GroupSingerControllerCsv.GetById(item.GroupSingerId).Name,
                    GroupComposedName = GroupComposedControllerCsv.GetById(item.GroupComposedId).Name,
                    Like = item.Like,
                    Image = item.Banner

                }).AsQueryable();
                var list = result?.Skip(skip).Take(next).ToList();
                return Tuple.Create(list, result?.Count());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public static Device getDevice(List<Device> device, Item item)
        {
            return device.FirstOrDefault(x => x.Id == item.DeviceId);
        }
        public static DeviceItem getDeviceItem(List<DeviceItem> deviceItems, Item item)
        {
            return deviceItems.FirstOrDefault(x => x.Id == item.DeviceItemId);
        }
        
        public static SelectItemModel GetOderByItemId(int? id)
        {

            try
            {
                var dl = GetByItemId(id);
                var dl1 = DeviceItemsController.GetById(dl.DeviceItemId);
                var dl2 = DevicesController.GetById(dl.DeviceId);
                var selectItemModel = new SelectItemModel
                {
                    COM = dl2.COM,
                    Order = dl1.Order
                };
                return selectItemModel;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        public static bool Insert(Items model)
        {
            try
            {
                _ = CsvDTO.Write<Items>(model.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Edit(Items model)
        {

            try
            {
                var get = CsvDTO.ListItems.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return false;
                }
                CsvDTO.ListItems.Remove(get);
                get.Banner = model.Banner;
                get.Description = model.Description;
                get.DeviceId = model.DeviceId;
                get.DeviceItemId = model.DeviceItemId;
                get.Name = model.Name;
                get.Note = model.Note;
                get.GroupItemId = model.GroupItemId;
                get.FriendlyName = model.FriendlyName;
                get.GroupComposedId = model.GroupComposedId;
                get.GroupSingerId = model.GroupSingerId;
                get.Like = model.Like;

                CsvDTO.ListItems.Add(get);
                CsvDTO.ListItems = CsvDTO.ListItems.OrderBy(x => x.Id).ToList();

                _ = CsvDTO.Update<Items>(CsvDTO.ListItems);
                return true;

            }
            catch
            {
                return false;
            }


        }

        public static bool Delete(int id)
        {
            try
            {
                var get = CsvDTO.ListItems.FirstOrDefault(x => x.Id == id);
                CsvDTO.ListItems.Remove(get);
                _ = CsvDTO.Update<Items>(CsvDTO.ListItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
