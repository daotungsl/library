﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.Data.Model;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;

namespace TN.Shared.DTO_New_Version.DTO
{
    public class DeviceItemsControllerCsv
    {
        public static List<DeviceItems> GetAll()
        {
            return CsvDTO.ListDeviceItems.Count > 0 ? CsvDTO.ListDeviceItems : null;
        }
        public static List<DeviceItemsModel> GetAllById(int? id)
        {
            var newItems = new List<DeviceItemsModel>();

            
                try
                {
                    var dl = CsvDTO.ListDeviceItems.Where(x=> x.DeviceId== id).ToList();
                    var items = CsvDTO.ListItems.Where(x => x.DeviceId == id).Select(x=> new { DeviceId = x.DeviceId, Id = x.Id, x.DeviceItemId }).ToList();
                    foreach(var item in dl)
                    {
                        if(items.Any(x=>x.DeviceItemId==item.Id))
                        {
                            newItems.Add(new DeviceItemsModel { Id = item.Id, Name = $"{item.Order} (Đang sử dụng)" });
                        }
                        else
                        {
                            newItems.Add(new DeviceItemsModel { Id = item.Id, Name = item.Order.ToString() });
                        }
                    }
                } 
                catch(Exception ex)
                {
                   
                }
            
            return newItems;
        }
        public static List<DeviceItemsModel> GetAllByName(string name)
        {
            var newItems = new List<DeviceItemsModel>();
            try
            {
                var getDevices = CsvDTO.ListDevices.First(x => x.Name == name);
                var dl = CsvDTO.ListDeviceItems.Where(x => x.DeviceId == getDevices.Id).ToList();
                var items = CsvDTO.ListItems.Where(x => x.DeviceId == getDevices.Id).Select(x => new { DeviceId = x.DeviceId, Id = x.Id, x.DeviceItemId }).ToList();
                foreach (var item in dl)
                {
                    if (items.Any(x => x.DeviceItemId == item.Id))
                    {
                        newItems.Add(new DeviceItemsModel { Id = item.Id, Name = $"{item.Order} (Đang sử dụng)" });
                    }
                    else
                    {
                        newItems.Add(new DeviceItemsModel { Id = item.Id, Name = item.Order.ToString() });
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return newItems;
        }
        public static DeviceItemsModel GetById(int? id)
        {
          var deviceItem = CsvDTO.ListDeviceItems.FirstOrDefault(x => x.Id == id);
          return new DeviceItemsModel{Id = deviceItem.Id,DeviceId = deviceItem.DeviceId,Order = deviceItem.Order,Status = deviceItem.Status, Name = $"{deviceItem.Order} (Đang sử dụng)" };
        }
        
        public static DeviceItems GetByOrder(int? order, int? deviceId)
        {
           
                return CsvDTO.ListDeviceItems.FirstOrDefault(x => x.Order == order && x.DeviceId == deviceId);
            
        }
        public static List<DeviceItems> GetByDeviceId(int deviceId)
        {
            
                return CsvDTO.ListDeviceItems.Where(x => x.DeviceId == deviceId).ToList();
            
        }

        public static bool Insert(DeviceItems model)
        {
            try
            {
                _ = CsvDTO.Write<DeviceItems>(model.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static DeviceItems Edit(DeviceItems model)
        {
           
                var get = CsvDTO.ListDeviceItems.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }

                CsvDTO.ListDeviceItems.Remove(get);
                get.Order = model.Order;
                get.Status = model.Status;
                CsvDTO.ListDeviceItems.Add(get);
                CsvDTO.ListDeviceItems = CsvDTO.ListDeviceItems.OrderBy(x => x.Id).ToList();

                _ = CsvDTO.Update<DeviceItems>(CsvDTO.ListDeviceItems);
            return model;
            
        }

        public static bool Delete(int id)
        {
            try
            {
                var get = CsvDTO.ListDeviceItems.FirstOrDefault(x => x.Id == id);
                CsvDTO.ListDeviceItems.Remove(get);
                _ = CsvDTO.Update<DeviceItems>(CsvDTO.ListDeviceItems);
                return true;
            }
            catch
            {
                return false;
            }
        }

        
    }
}
