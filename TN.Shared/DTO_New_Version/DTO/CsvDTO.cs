﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.DTO_New_Version.EntityCsv;
using GroupComposed = TN.Shared.DTO_New_Version.EntityCsv.GroupComposed;
using GroupSinger = TN.Shared.DTO_New_Version.EntityCsv.GroupSinger;
using History = TN.Shared.DTO_New_Version.EntityCsv.History;

namespace TN.Shared.DTO_New_Version.DTO
{
    public enum ClassType
    {
        DeviceItems = 1,
        Devices = 2,
        GroupComposed = 3,
        GroupItems = 4,
        GroupSinger = 5,
        History = 6,
        Items = 7
    }

    public class CsvDTO
    {
        public static List<DeviceItems> ListDeviceItems = null;
        public static List<Devices> ListDevices = null;
        public static List<GroupComposed> ListGroupComposed = null;
        public static List<GroupItems> ListGroupItems = null;
        public static List<GroupSinger> ListGroupSinger = null;
        public static List<History> ListHistory = null;
        public static List<Items> ListItems = null;

        #region Update

        public static async Task Update<T>(List<T> list)
        {
            var directory = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var path = "";
            var csv = new StringBuilder();

            foreach (var item in list)
            {
                csv.AppendLine(item.ToString());
            }

            var checkUpdate = checkCreate;
            checkCreate = true;

            switch (typeof(T).Name)
            {
                case "DeviceItems":
                    path = CreateFile("DeviceItems.csv");
                    //File.WriteAllText(path, DeviceItems.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, DeviceItems.ToHeader());

                    await ReadDeviceItems();
                    break;
                case "Devices":
                    path = CreateFile("Devices.csv");
                    //File.WriteAllText(path, Devices.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, Devices.ToHeader());

                    await ReadDevices();
                    break;
                case "GroupComposed":
                    path = CreateFile("GroupComposed.csv");
                    //File.WriteAllText(path, GroupComposed.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, GroupComposed.ToHeader());

                    await ReadGroupComposed();
                    break;
                case "GroupItems":
                    path = CreateFile("GroupItems.csv");
                    //File.WriteAllText(path, GroupItems.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, GroupItems.ToHeader());

                    await ReadGroupItems();
                    break;
                case "GroupSinger":
                    path = CreateFile("GroupSinger.csv");
                    //File.WriteAllText(path, GroupSinger.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, GroupSinger.ToHeader());
                    await ReadGroupSinger();
                    break;
                case "History":
                    path = CreateFile("History.csv");
                    //File.WriteAllText(path, History.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, History.ToHeader());
                    await ReadHistory();
                    break;
                case "Items":
                    path = CreateFile("Items.csv");
                    //File.WriteAllText(path, Items.ToHeader() + Environment.NewLine, Encoding.UTF8);
                    //File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
                    DynamicHeader(path, csv, Items.ToHeader());
                    await ReadItems();
                    break;
                default:
                    await ReadAllAsync();
                    break;
            }

            checkCreate = checkUpdate;
        }

        #endregion

        #region WriteFile

        public static async Task Write<T>(List<T> data)
        {
            var directory = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var path = "";

            var csv = new StringBuilder();
            foreach (var item in data)
            {
                csv.AppendLine(item.ToString());
            }

            switch (typeof(T).Name)
            {
                case "DeviceItems":
                    path = CreateFile("DeviceItems.csv");
                    DynamicHeader(path, csv, DeviceItems.ToHeader());
                    await ReadDeviceItems();
                    break;
                case "Devices":
                    path = CreateFile("Devices.csv");
                    DynamicHeader(path, csv, Devices.ToHeader());
                    await ReadDevices();
                    break;
                case "GroupComposed":
                    path = CreateFile("GroupComposed.csv");
                    DynamicHeader(path, csv, GroupComposed.ToHeader());
                    await ReadGroupComposed();
                    break;
                case "GroupItems":
                    path = CreateFile("GroupItems.csv");
                    DynamicHeader(path, csv, GroupItems.ToHeader());
                    await ReadGroupItems();
                    break;
                case "GroupSinger":
                    path = CreateFile("GroupSinger.csv");
                    DynamicHeader(path, csv, GroupSinger.ToHeader());
                    await ReadGroupSinger();
                    break;
                case "History":
                    path = CreateFile("History.csv");
                    DynamicHeader(path, csv, History.ToHeader());
                    await ReadHistory();
                    break;
                case "Items":
                    path = CreateFile("Items.csv");
                    DynamicHeader(path, csv, Items.ToHeader());
                    await ReadItems();
                    break;
                default:
                    await ReadAllAsync();
                    break;
            }
        }

        private static void DynamicHeader(string path, StringBuilder csv, string header)
        {
            if (checkCreate)
            {
                File.WriteAllText(path, header + Environment.NewLine, Encoding.UTF8);
                File.AppendAllText(path, csv.ToString(), Encoding.UTF8);
            }
        }

       
        public static async Task Write<T>(string data)
        {
            var directory = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var path = "";

            switch (typeof(T).Name)
            {
                case "DeviceItems":
                    path = CreateFile("DeviceItems.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadDeviceItems();
                    break;
                case "Devices":
                    path = CreateFile("Devices.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadDevices();
                    break;
                case "GroupComposed":
                    path = CreateFile("GroupComposed.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadGroupComposed();
                    break;
                case "GroupItems":
                    path = CreateFile("GroupItems.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadGroupItems();
                    break;
                case "GroupSinger":
                    path = CreateFile("GroupSinger.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadGroupSinger();
                    break;
                case "History":
                    path = CreateFile("History.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadHistory();
                    break;
                case "Items":
                    path = CreateFile("Items.csv");
                    File.AppendAllText(path, data + Environment.NewLine, Encoding.UTF8);
                    await ReadItems();
                    break;
                default:
                    await ReadAllAsync();
                    break;
            }
        }

        #endregion

        #region ReadFile
        public static async Task ReadAllAsync()
        {
            try
            {
                var path = $@"{Directory.GetCurrentDirectory()}\Data";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var pathImg = $@"{path}\Image";
                if (!Directory.Exists(pathImg))
                {
                    Directory.CreateDirectory(pathImg);
                }

                var pathDeviceItems = CreateFile("DeviceItems.csv");
                var pathDevices = CreateFile("Devices.csv");
                var pathGrComposed = CreateFile("GroupComposed.csv");
                var pathGrItems = CreateFile("GroupItems.csv");
                var pathGrSinger = CreateFile("GroupSinger.csv");
                var pathHistory = CreateFile("History.csv");
                var pathItems = CreateFile("Items.csv");

                ListDeviceItems = await ReadAsync<DeviceItems>(pathDeviceItems);
                ListDevices = await ReadAsync<Devices>(pathDevices);
                ListGroupComposed = await ReadAsync<GroupComposed>(pathGrComposed);
                ListGroupItems = await ReadAsync<GroupItems>(pathGrItems);
                ListGroupSinger = await ReadAsync<GroupSinger>(pathGrSinger);
                ListHistory = await ReadAsync<History>(pathHistory);
                ListItems = await ReadAsync<Items>(pathItems);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public static async Task ReadAllAsyncWithPath(string pathDeviceItems, string pathDevices, string pathGrComposed, string pathGrItems, string pathGrSinger, string pathHistory, string pathItems)
        {
            if (pathDeviceItems != null && File.Exists(pathDeviceItems))
            {
                ListDeviceItems = await ReadAsync<DeviceItems>(pathDeviceItems);
            }

            if (pathDevices != null && File.Exists(pathDevices))
            {
                ListDevices = await ReadAsync<Devices>(pathDevices);
            }

            if (pathGrComposed != null && File.Exists(pathGrComposed))
            {
                ListGroupComposed = await ReadAsync<GroupComposed>(pathGrComposed);
            }

            if (pathGrItems != null && File.Exists(pathGrItems))
            {
                ListGroupItems = await ReadAsync<GroupItems>(pathGrItems);
            }

            if (pathGrSinger != null && File.Exists(pathGrSinger))
            {
                ListGroupSinger = await ReadAsync<GroupSinger>(pathGrSinger);
            }

            if (pathHistory != null && File.Exists(pathHistory))
            {
                ListHistory = await ReadAsync<History>(pathHistory);
            }

            if (pathItems != null && File.Exists(pathItems))
            {
                ListItems = await ReadAsync<Items>(pathItems);
            }
        }
        public static async Task ReadDeviceItems()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathDeviceItems = CreateFile("DeviceItems.csv");
            ListDeviceItems = await ReadAsync<DeviceItems>(pathDeviceItems);
        }
        public static async Task ReadDevices()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathDevices = CreateFile("Devices.csv");
            ListDevices = await ReadAsync<Devices>(pathDevices);
        }
        public static async Task ReadGroupComposed()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathGrComposed = CreateFile("GroupComposed.csv");
            ListGroupComposed = await ReadAsync<GroupComposed>(pathGrComposed);
        }
        public static async Task ReadGroupItems()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathGrItems = CreateFile("GroupItems.csv");
            ListGroupItems = await ReadAsync<GroupItems>(pathGrItems);
        }
        public static async Task ReadGroupSinger()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathGrSinger = CreateFile("GroupSinger.csv");
            ListGroupSinger = await ReadAsync<GroupSinger>(pathGrSinger);
        }
        public static async Task ReadHistory()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathHistory = CreateFile("History.csv");
            ListHistory = await ReadAsync<History>(pathHistory);
        }
        public static async Task ReadItems()
        {
            var path = $@"{Directory.GetCurrentDirectory()}\Data";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathItems = CreateFile("Items.csv");
            ListItems = await ReadAsync<Items>(pathItems);
        }

        #endregion

        #region SetValue

        internal static async Task<List<T>> ReadAsync<T>(string path)
        {
            
            var file = new FileInfo(path);
            var list = new List<T>();
            var map = Mapping<T>();
            var converters = new List<IConverter>();

            using (var stream = file.OpenText())
            {
                string line;
                while ((line = await stream.ReadLineAsync().ConfigureAwait(false)) != null)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(line) || line.Contains("Id")) continue;
                        if (line.LastIndexOf(",", StringComparison.Ordinal) >= 0 && line.LastIndexOf(",", StringComparison.Ordinal) == line.Length - 1)
                            line = line.Remove(line.Length - 1);
                        if (line.LastIndexOf(";", StringComparison.Ordinal) >= 0 && line.LastIndexOf(";", StringComparison.Ordinal) == line.Length - 1)
                            line = line.Remove(line.Length - 1);
                        if (line.LastIndexOf("|", StringComparison.Ordinal) >= 0 && line.LastIndexOf("|", StringComparison.Ordinal) == line.Length - 1)
                            line = line.Remove(line.Length - 1);
                        var data = line.Split(',');
                        var obj = Activator.CreateInstance<T>();

                        foreach (var item in map)
                        {
                            var value = data[item.Item2.Position];
                            if (item.Item2.Converter != null)
                            {
                                var converter = GetMatchingConverter(converters, item.Item2.Converter);
                                item.Item1.SetValue(obj, converter.Convert(value));
                            }
                            else
                            {
                                SetValue(obj, item.Item1, value);
                            }
                        }
                        list.Add(obj);
                    }
                    catch (Exception e)
                    {
                        //
                    }
                }
            }

            return list;
        }

        internal static void SetValue(object obj, PropertyInfo prop, object value)
        {

            if (prop.PropertyType == typeof(int?))
            {
                int tmp;
                if (int.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else if (prop.PropertyType == typeof(byte?))
            {
                byte tmp;
                if (byte.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else if (prop.PropertyType == typeof(long?))
            {
                long tmp;
                if (long.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else if (prop.PropertyType == typeof(float?))
            {
                float tmp;
                if (float.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else if (prop.PropertyType == typeof(double?))
            {
                double tmp;
                if (double.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else if (prop.PropertyType == typeof(DateTime?))
            {
                DateTime tmp;
                if (DateTime.TryParse(value as string, out tmp))
                    prop.SetValue(obj, tmp);
            }
            else
            {
                prop.SetValue(obj, Convert.ChangeType(value, prop.PropertyType));
            }

        }

        internal static IConverter GetMatchingConverter(IList<IConverter> converters, Type objectType)
        {
            IConverter converter;
            if (converters != null)
            {
                foreach (var t in converters)
                {
                    converter = t;
                    if (converter.GetType() == objectType)
                        return converter;
                }
            }
            converter = (IConverter)Activator.CreateInstance(objectType);
            converters.Add(converter);
            return converter;
        }

        internal static List<Tuple<PropertyInfo, PositionAttribute>> Mapping<T>()
        {
            var list = new List<Tuple<PropertyInfo, PositionAttribute>>();
            var properties = typeof(T).GetProperties();
            foreach (var pInfo in properties)
            {
                var attr = pInfo.GetCustomAttribute<PositionAttribute>();
                if (attr != null)
                    list.Add(new Tuple<PropertyInfo, PositionAttribute>(pInfo, attr));
            }
            return list;
        }
        #endregion

        #region Utilities

        internal static bool checkCreate = false;

        internal static string CreateFile(string fileName)
        {
            try
            {
                var directory = $@"{Directory.GetCurrentDirectory()}\Data";
                var path = Path.Combine(directory, fileName);

                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                    checkCreate = true;
                }
                return path;
            }
            catch (Exception e)
            {
                //
                return $@"{Directory.GetCurrentDirectory()}\Data\{fileName}";
            }
        }

        internal static bool IsFileLocked(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                try
                {
                    stream?.Close();
                }
                catch
                {

                }
                return true;
            }
            finally
            {
                stream?.Close();
            }
            return false;
        }

        #endregion
    }

    internal class PositionAttribute : Attribute
    {
        public int Position { get; set; }
        public Type Converter { get; set; }

        public PositionAttribute(int position)
        {
            Position = position;
        }
    }

    internal interface IConverter
    {
        object Convert(object value);
    }
}
