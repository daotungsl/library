﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace TN.Shared.DTO_New_Version.DTO
{
    public class GroupItemsControllerCsv
    {
        public static List<GroupItems> GetAll()
        {
            return CsvDTO.ListGroupItems?.Count > 0 ? CsvDTO.ListGroupItems : null;
        }
        public static GroupItems GetByName(string name)
        {

            return CsvDTO.ListGroupItems.FirstOrDefault(x => x.Name == name);
            
        }
        public static GroupItems GetById(int id)
        {

            return CsvDTO.ListGroupItems.FirstOrDefault(x => x.Id == id);

        }

        public static bool Insert(GroupItems model)
        {
            try
            {
                _ = CsvDTO.Write<GroupItems>(model.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static GroupItem Edit(GroupItem model)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupItems.FirstOrDefault(x => x.Id == model.Id);
            //    if (get == null)
            //    {
            //        return null;
            //    }
            //    get.Name = model.Name;
            //    db.SubmitChanges();
            //    return model;
            //}
            return null;
        }

        public static void Delete(int id)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupItems.FirstOrDefault(x => x.Id == id);
            //    if (get != null)
            //    {
            //        db.GroupItems.DeleteOnSubmit(get);
            //        db.SubmitChanges();
            //    }
            //}

        }


    }
}
