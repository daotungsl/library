﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;
using TN.Shared.DTO_New_Version.Model;

namespace TN.Shared.DTO_New_Version.DTO
{
    public class DevicesControllerCsv
    {
        public static List<Devices> GetAll()
        {
            return CsvDTO.ListDevices.Count > 0 ? CsvDTO.ListDevices : null;
        }

        public static Devices GetById(int? id)
        {
            return CsvDTO.ListDevices.FirstOrDefault(x => x.Id == id);
        }
        public static Devices GetByImei(string imei)
        {
            return CsvDTO.ListDevices.FirstOrDefault(x => x.IMEI == imei);
        }
        public static Devices GetByName(string name)
        {
            return CsvDTO.ListDevices.FirstOrDefault(x => x.Name == name);
        }
        public static Devices GetByPort(int? port)
        {
            return CsvDTO.ListDevices.FirstOrDefault(x => x.PORT == port);
        }
        public static List<DevicesModel> GetAllWithTray()
        {

            
                var dl1 = CsvDTO.ListDeviceItems.GroupBy(x => x.Id).Select(x => new { DeviceId = x.Key, Count = x.Count() });
                var list = GetAll().Select(item => new DevicesModel()
                {
                    Id = item.Id,
                    IMEI = item.IMEI,
                    IP = item.IP,
                    Name = item.Name,
                    Address = item.Address,
                    PORT = item.PORT,
                    Tray = dl1.FirstOrDefault(x => x.DeviceId == item.Id) == null ? 0 : dl1.FirstOrDefault(x => x.DeviceId == item.Id).Count,
                    COM = item.COM,
                    Status = item.Status
                });
                return list.ToList();
            
        }
        public static Tuple<List<DevicesModel>, int> FullTextFind(
            int skip, int next,
            string key
            )
        {
          
                var dl = CsvDTO.ListDeviceItems.GroupBy(x => x.DeviceId).Select(x => new { DeviceId = x.Key, Count = x.Count() });

                var qr = CsvDTO.ListDevices.AsQueryable();
                if (key != null)
                {
                    qr = CsvDTO.ListDevices.Where(x =>
                 (x.IMEI.Contains(key) ||
                 x.IP.Contains(key) ||
                 x.Name.Contains(key) ||
                 x.Address.Contains(key) ||
                 x.Description.Contains(key) ||
                 x.COM.Contains(key) ||
                 key == null)
                ).AsQueryable();
                }
                var qr1 = qr.Skip(skip).Take(next).Select(item => new DevicesModel
                {

                    Id = item.Id,
                    IMEI = item.IMEI,
                    Description = item.Description,
                    Name = item.Name,
                    COM = item.COM,
                    Address = item.Address,
                    IP = item.IP,
                    PORT = item.PORT,
                    Tray = dl.FirstOrDefault(x => x.DeviceId == item.Id) == null ? 0 : dl.FirstOrDefault(x => x.DeviceId == item.Id).Count,
                    Status = item.Status
                }).AsQueryable();
                try
                {

                    var list = qr1.ToList();
                    return Tuple.Create(list, qr.Count());
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }

                return null;

            
        }

        public static bool Insert(Devices model)
        {
            try
            {
                _ = CsvDTO.Write<Devices>(model.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Edit(Devices model)
        {
            try
            {
                var get = CsvDTO.ListDevices.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return false;
                }
                CsvDTO.ListDevices.Remove(get);
                

                CsvDTO.ListDevices.Add(get);
                CsvDTO.ListDevices = CsvDTO.ListDevices.OrderBy(x => x.Id).ToList();

                _ = CsvDTO.Update<Devices>(CsvDTO.ListDevices);
                return true;

            }
            catch
            {
                return false;
            }
        }
        public static bool EditList(List<Devices> list)
        {
            try
            {
                CsvDTO.ListDevices.Clear();

                _ = CsvDTO.Update<Devices>(list);
                return true;

            }
            catch
            {
                return false;
            }
        }

        public static void Delete(int id)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.Devices.FirstOrDefault(x => x.Id == id);
            //    if (get != null)
            //    {
            //        var listDeviceItems = DeviceItemsController.GetByDeviceId(id);
            //        //var listDeviceItems = db.DeviceItems.Where(DI => DI.DeviceId.Equals(get.Id)).ToList();

            //        foreach (var deviceItem in listDeviceItems)
            //        {
            //            var listItem = db.Items
            //                .Where(i => i.DeviceId.Equals(get.Id) && i.DeviceItemId.Equals(deviceItem.Id)).ToList();
            //            foreach (var item in listItem)
            //            {
            //                ItemsController.Delete(item.Id);
            //                //db.Items.DeleteOnSubmit(item);
            //            }
            //            DeviceItemsController.Delete(deviceItem.Id);
            //            //db.DeviceItems.DeleteOnSubmit(deviceItem);
            //        }
            //        db.Devices.DeleteOnSubmit(get);
            //        db.SubmitChanges();
            //    }
            //}
        }
    }
}
