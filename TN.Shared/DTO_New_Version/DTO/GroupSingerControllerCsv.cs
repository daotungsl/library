﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace TN.Shared.DTO_New_Version.DTO
{
    public class GroupSingerControllerCsv
    {

        public static List<GroupSinger> GetAll()
        {
            return CsvDTO.ListGroupSinger.ToList();
        }
        public static GroupSinger GetByName(string name)
        {
            return CsvDTO.ListGroupSinger.FirstOrDefault(x => x.Name == name);

        }


        public static GroupSinger GetById(int? id)
        {

            return CsvDTO.ListGroupSinger.FirstOrDefault(x => x.Id == id);

        }

        public static bool Insert(GroupSinger model)
        {
            try
            {
                _ = CsvDTO.Write<GroupSinger>(model.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static GroupSinger Edit(GroupSinger model)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupSingers.FirstOrDefault(x => x.Id == model.Id);
            //    if (get == null)
            //    {
            //        return null;
            //    }
            //    get.Name = model.Name;
            //    get.FriendlyName = model.FriendlyName;
            //    db.SubmitChanges();
            //    return model;
            //}
            return null;

        }

        public static void Delete(int id)
        {
            //using (var db = new LibraryDataContext())
            //{
            //    var get = db.GroupSingers.FirstOrDefault(x => x.Id == id);
            //    if (get != null)
            //    {
            //        db.GroupSingers.DeleteOnSubmit(get);
            //        db.SubmitChanges();
            //    }
            //}
        }

       
    }
}
