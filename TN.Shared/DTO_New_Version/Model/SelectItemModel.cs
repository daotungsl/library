﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Shared.DTO_New_Version.Model
{
   public class SelectItemsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string COM { get; set; }
        public int Order { get; set; }

    }
}
