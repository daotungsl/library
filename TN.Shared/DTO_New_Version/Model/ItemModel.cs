﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.EntityCsv;

namespace TN.Shared.DTO_New_Version.Model
{
   public class ItemsModel : Items
    {
        public string Image { get; set; }
        public string GroupSingerName { get; set; }
        public string GroupComposedName { get; set; }
        private string _DeviceName;
        private string _DeviceItemName;
        private string _GroupItemName;
        private int _Status;
        public string DeviceName
        {
            get
            {
                return this._DeviceName;
            }
            set
            {
                if ((this._DeviceName != value))
                {

                    this._DeviceName = value;

                }
            }
        }
        public string DeviceItemName
        {
            get
            {
                return this._DeviceItemName;
            }
            set
            {
                if ((this._DeviceItemName != value))
                {

                    this._DeviceItemName = value;

                }
            }
        }
        public string GroupItemName
        {
            get
            {
                return this._GroupItemName;
            }
            set
            {
                if ((this._GroupItemName != value))
                {

                    this._GroupItemName = value;

                }
            }
        }
        public int Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                if ((this._Status != value))
                {

                    this._Status = value;

                }
            }
        }
    }
}
