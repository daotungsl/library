﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;

namespace TN.Shared.DTO_New_Version.EntityCsv
{
	public class DeviceItems
    {
        [Position(0)]
        public int Id { get; set; }
        [Position(1)]
        public int DeviceId { get; set; }
        [Position(2)]
        public int Order { get; set; }
        [Position(3)]
        public int Status { get; set; }

        public DeviceItems()
        {
            
        }

        public DeviceItems(int id, int deviceId, int order, int status)
        {
            Id = id;
            DeviceId = deviceId;
            Order = order;
            Status = status;
        }

        public override string ToString()
        {
            return $"{Id},{DeviceId},{Order},{Status}";
        }

        public static string ToHeader()
        {
            return $"Id,DeviceId,Order,Status";
        }
    }
}
