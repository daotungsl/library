﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;

namespace TN.Shared.DTO_New_Version.EntityCsv
{
	public class GroupItems
    {
        [Position(0)]
        public int Id { get; set; }
        [Position(1)]
        public string Name { get; set; }
        [Position(2)]
        public DateTime CreatedDate { get; set; }
        [Position(3)]
        public int CreatedUser { get; set; }
        [Position(4)]
        public string FriendlyName { get; set; }

        public GroupItems()
        {
            
        }

        public GroupItems(int id, string name, DateTime createdDate, int createdUser, string friendlyName)
        {
            Id = id;
            Name = name;
            CreatedDate = createdDate;
            CreatedUser = createdUser;
            FriendlyName = friendlyName;
        }

        public override string ToString()
        {
            return $"{Id},{Name},{CreatedDate},{CreatedUser},{FriendlyName}";
        }

        public static string ToHeader()
        {
            return $"Id,Name,CreatedDate,CreatedUser,FriendlyName";
        }
    }
}
