﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;

namespace TN.Shared.DTO_New_Version.EntityCsv
{
    public class Items
    {
        [Position(0)]
        public int Id { get; set; }
        [Position(1)]
        public int GroupItemId { get; set; }
        [Position(2)]
        public string Name { get; set; }
        [Position(3)]
        public string Banner { get; set; }
        [Position(4)]
        public string Description { get; set; }
        [Position(5)]
        public string Note { get; set; }
        [Position(6)]
        public int DeviceId { get; set; }
        [Position(7)]
        public int DeviceItemId { get; set; }
        [Position(8)]
        public int GroupSingerId { get; set; }
        [Position(9)]
        public int GroupComposedId { get; set; }
        [Position(10)]
        public string FriendlyName { get; set; }
        [Position(11)]
        public int Like { get; set; }
        [Position(12)]
        public DateTime LastOpen { get; set; }
        public Items()
        {
            
        }

        public Items(int id, int groupItemId, string name, string banner, string description, string note, int deviceId, int deviceItemId, int groupSingerId, int groupComposedId, string friendlyName, int like, DateTime lastOpen)
        {
            Id = id;
            GroupItemId = groupItemId;
            Name = name;
            Banner = banner;
            Description = description;
            Note = note;
            DeviceId = deviceId;
            DeviceItemId = deviceItemId;
            GroupSingerId = groupSingerId;
            GroupComposedId = groupComposedId;
            FriendlyName = friendlyName;
            Like = like;
            LastOpen = lastOpen;
        }

        public override string ToString()
        {
            return $"{Id},{GroupItemId},{Name},{Banner},{Description},{Note},{DeviceId},{DeviceItemId},{GroupSingerId},{GroupComposedId},{FriendlyName},{Like},{LastOpen}";
        }

        public static string ToHeader()
        {
            return $"Id,GroupItemId,Name,Banner,Description,Note,DeviceId,DeviceItemId,GroupSingerId,GroupComposedId,FriendlyName,Like,LastOpen";
        }
    }
}
