﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;

namespace TN.Shared.DTO_New_Version.EntityCsv
{
	public class History
    {
        [Position(0)]
        public int Id { get; set; }
        [Position(1)]
        public int ItemId { get; set; }
        [Position(2)]
        public DateTime CreatedAt { get; set; }

        public History()
        {
            
        }

        public History(int id, int itemId, DateTime createdAt)
        {
            Id = id;
            ItemId = itemId;
            CreatedAt = createdAt;
        }

        public override string ToString()
        {
            return $"{Id},{ItemId},{CreatedAt}";
        }

        public static string ToHeader()
        {
            return $"Id,ItemId,CreatedAt";
        }
    }
}
