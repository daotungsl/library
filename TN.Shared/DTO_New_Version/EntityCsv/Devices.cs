﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.DTO_New_Version.DTO;

namespace TN.Shared.DTO_New_Version.EntityCsv
{
	public class Devices
    {
        [Position(0)]
        public int Id { get; set; }
        [Position(1)]
        public string IMEI { get; set; }
        [Position(2)]
        public string Name { get; set; }
        [Position(3)]
        public string IP { get; set; }
        [Position(4)]
        public int PORT { get; set; }
        [Position(5)]
        public string Address { get; set; }
        [Position(6)]
        public string Description { get; set; }
        [Position(7)]
        public int Status { get; set; }
        [Position(8)]
        public string COM { get; set; }

        public Devices()
        {
            
        }

        public Devices(int id, string imei, string name, string ip, int port, string address, string description, int status, string com)
        {
            Id = id;
            IMEI = imei;
            Name = name;
            IP = ip;
            PORT = port;
            Address = address;
            Description = description;
            Status = status;
            COM = com;
        }

        public override string ToString()
        {
            return $"{Id},{IMEI},{Name},{IP},{PORT},{Address},{Description},{Status},{COM}";
        }

        public static string ToHeader()
        {
            return $"Id,IMEI,Name,IP,PORT,Address,Description,Status,COM";
        }
    }
}
