﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.Data.Model;
namespace TN.Shared.DTO
{

    public class ItemsController
    {
        
        public static List<Item> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.Items.ToList();
            }
        }

        public static Item GetByItemId(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Items.FirstOrDefault(x => x.Id == id);
            }
        }

        public static Tuple<List<ItemModel>, int> GetAllWithInfo(int _Skip, int _Next)
        {
            using (var db = new LibraryDataContext())
            {
                var qr = GetAll().Select(item => new ItemModel
                {
                    Id = item.Id,
                    Banner = item.Banner,
                    Description = item.Description,
                    Name = item.Name,
                    Note = item.Note,
                    DeviceName = DevicesController.GetById(item.DeviceId).Name,
                    DeviceItemName = DeviceItemsController.GetById(item.DeviceItemId).Order.ToString(),
                    GroupItemName = GroupItemsController.GetById(item.GroupItemId).Name,
                    Status = DeviceItemsController.GetById(item.DeviceItemId).Status,
                    Image = System.Drawing.Image.FromFile(item.Banner)

                }).AsQueryable();
                var list = qr.Skip(_Skip).Take(_Next).ToList();
                return Tuple.Create(list, qr.Count());
            }
        }
        public static Tuple<List<ItemModel>, int> FullTextFind(
            int skip, int next,
            int? deviceId,
            int? deviceItemId,
            int? groupItemId,
            int? itemStatus,
            string key
            )
        {
            using (var db = new LibraryDataContext())
            {

                var qr = db.Items.AsQueryable();
                if (deviceId != null || deviceItemId != null || groupItemId != null || itemStatus != null || key != null)
                {
                    qr = db.Items.Where(x =>
                 //(itemStatus == null || db.DeviceItems.Any(m => m.Id == x.DeviceItemId && m.Status == itemStatus)) &&
                 (x.DeviceId == deviceId || deviceId == null) &&
                 (x.DeviceItemId == deviceItemId || deviceItemId == null) &&
                 (x.Banner.Contains(key) ||
                 x.Description.Contains(key) ||
                 x.Note.Contains(key) ||
                 x.Name.Contains(key) ||
                 db.GroupSingers.Any(m => m.Id == x.GroupSingerId && m.Name.Contains(key)) ||
                 db.GroupComposeds.Any(m => m.Id == x.GroupComposedId && m.Name.Contains(key)) ||
                 key == null) &&
                 (x.GroupItemId == groupItemId || groupItemId == null)
                ).AsQueryable();
                }
               
                try
                {
                    var qr1 = qr.Skip(skip).Take(next).Select(item => new ItemModel
                    {

                        Id = item.Id,
                        Banner = item.Banner,
                        Description = item.Description,
                        Name = item.Name,
                        Note = item.Note,
                        DeviceName = DevicesController.GetById(item.DeviceId).Name,
                        DeviceItemName = DeviceItemsController.GetById(item.DeviceItemId).Order.ToString(),
                        GroupItemName = GroupItemsController.GetById(item.GroupItemId).Name,
                        GroupComposedName = GroupComposedController.GetById(item.GroupComposedId).Name,
                        GroupSingerName = GroupSingerController.GetById(item.GroupSingerId).Name,
                        Status = DeviceItemsController.GetById(item.DeviceItemId).Status,
                        //Image = System.Drawing.Image.FromFile(item.Banner)
                    }).AsQueryable();
                    var list = qr1.ToList();
                    return Tuple.Create(list, qr.Count());
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }

                return null;

            }
        }
        public static Tuple<List<ItemModel>, int> FullTextFindByBtn(
            int skip, int next,
            string fullText,
            string name,
            string singer,
            string composed,
            string kid,
            bool history,
            int? deviceId,
            int? deviceItemId,
            int? groupItemId,
            int? itemStatus,
            string key
            )
        {
            var _Devices = DevicesController.GetAll();
            var _DeviceItems = DeviceItemsController.GetAll();
            var _GroupItems = GroupItemsController.GetAll();
            var _GroupComposed = GroupComposedController.GetAll();
            var _GroupSinger = GroupSingerController.GetAll();

            using (var db = new LibraryDataContext())
            {

                var qr = db.Items.AsQueryable();
                if (fullText != null)
                {

                    qr = db.Items.Where(x =>
                                             x.Banner.Contains(fullText) ||
                                             x.Description.Contains(fullText) ||
                                             x.Note.Contains(fullText) ||
                                             x.Name.Contains(fullText) ||
                                             db.GroupItems.Any(m => m.Id == x.GroupItemId && m.Name.Contains(fullText)) ||
                                             db.GroupSingers.Any(m => m.Id == x.GroupSingerId && m.Name.Contains(fullText)) ||
                                             db.GroupComposeds.Any(m => m.Id == x.GroupComposedId && m.Name.Contains(fullText)) ||
                                             fullText == null
                                            ).AsQueryable();

                }
                if (name != null)
                {
                    //qr = qr.Where(x => x.FriendlyName.Contains(name)).AsQueryable();
                    //lọc item theo kí tự bấm
                    qr = qr.Where(x => x.FriendlyName.Substring(0, name.Length).Equals(name)).AsQueryable();
                }
                if (singer != null)
                {
                    qr = db.Items.Where(x =>
                                             singer == null || 
                                             db.GroupSingers.Any(m => m.Id == x.GroupSingerId && m.FriendlyName.Contains(singer))).AsQueryable();
                }
                if (composed != null)
                {
                    qr = db.Items.Where(x =>
                                            composed == null ||
                                            db.GroupComposeds.Any(m => m.Id == x.GroupComposedId && m.FriendlyName.Contains(composed))).AsQueryable();

                }
                if (kid != null)
                {
                    qr = db.Items.Where(x =>
                                            kid == null ||
                                            db.GroupItems.Any(m => m.Id == x.GroupItemId && m.FriendlyName.Contains(kid))).AsQueryable();

                }
                if (history)
                {
                    qr = db.Items.Where(x => db.Histories.OrderByDescending(s => s.Id).Take(next).Any(m => m.ItemId == x.Id)).AsQueryable();
                    //qr = db.Items.Where(x => db.Histories.Any(m => m.ItemId == x.Id)).AsQueryable();
                    //var a = qr.ToList();

                }

                if (deviceId != null || deviceItemId != null || groupItemId != null || itemStatus != null || key != null)
                {
                    qr = db.Items.Where(x =>
                 //(itemStatus == null || db.DeviceItems.Any(m => m.Id == x.DeviceItemId && m.Status == itemStatus)) &&
                 (x.DeviceId == deviceId || deviceId == null) &&
                 (x.DeviceItemId == deviceItemId || deviceItemId == null) &&
                 (x.Banner.Contains(key) ||
                 x.Description.Contains(key) ||
                 x.Note.Contains(key) ||
                 x.Name.Contains(key) ||
                 db.GroupSingers.Any(m => m.Id == x.GroupSingerId && m.Name.Contains(key)) ||
                 db.GroupComposeds.Any(m => m.Id == x.GroupComposedId && m.Name.Contains(key)) ||
                 key == null) &&
                 (x.GroupItemId == groupItemId || groupItemId == null)
                ).AsQueryable();
                }
                try
                {
                    var qr1 = qr.Skip(skip).Take(next).Select(item => new ItemModel
                    {

                        Id = item.Id,
                        Banner = item.Banner,
                        Description = item.Description,
                        Name = item.Name,
                        Note = item.Note,
                        DeviceName = getDevice(_Devices, item).Name,
                        DeviceItemName = getDeviceItem(_DeviceItems, item).Order.ToString(),
                        GroupItemName = getGroupItem(_GroupItems, item).Name,
                        GroupComposedName = getGroupComposed(_GroupComposed, item).Name,
                        GroupSingerName = getGroupSinger(_GroupSinger, item).Name,
                        FriendlyName = item.FriendlyName
                        //Status = _DeviceItems.FirstOrDefault(x => x.Id == item.DeviceItemId).Status,
                        //Image = System.Drawing.Image.FromFile(item.Banner)
                    }).AsQueryable();
                    var list = qr1.ToList();
                    return Tuple.Create(list, qr.Count());
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }

                return null;

            }
        }
        public static Device getDevice(List<Device> device, Item item)
        {
            return device.FirstOrDefault(x => x.Id == item.DeviceId);
        }
        public static DeviceItem getDeviceItem(List<DeviceItem> deviceItems, Item item)
        {
            return deviceItems.FirstOrDefault(x => x.Id == item.DeviceItemId);
        }
        public static GroupItem getGroupItem(List<GroupItem> groupItems, Item item)
        {
            var dl = groupItems.FirstOrDefault(x => x.Id == item.GroupItemId);
            if (dl != null)
            {
                return dl;
            }
            else
            {
                try
                {
                    item.GroupItemId = 8;
                   ItemsController.Edit(item);
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }
                dl = GroupItemsController.GetById(8);
            }
            return dl;
        }
        public static GroupComposed getGroupComposed(List<GroupComposed> groupComposeds, Item item)
        {
            var dl = groupComposeds.FirstOrDefault(x => x.Id == item.GroupComposedId);
            if (dl != null)
            {
                return dl;
            }
            else
            {
                try
                {
                    item.GroupComposedId = 24;
                    ItemsController.Edit(item);
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }
                dl = GroupComposedController.GetById(24);
            }
            return dl;
        }
        public static GroupSinger getGroupSinger(List<GroupSinger> groupSingers, Item item)
        {
            var dl = groupSingers.FirstOrDefault(x => x.Id == item.GroupSingerId);
            if (dl != null)
            {
                return dl;
            }
            else
            {
                try
                {
                    item.GroupSingerId = 39;
                    ItemsController.Edit(item);
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }
                dl = GroupSingerController.GetById(39);
            }
            return dl;
        }
        public static SelectItemModel GetOderByItemId(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                try
                {
                    var dl = GetByItemId(id);
                    var dl1 = DeviceItemsController.GetById(dl.DeviceItemId);
                    var dl2 = DevicesController.GetById(dl.DeviceId);
                    var selectItemModel = new SelectItemModel
                    {
                        COM = dl2.COM,
                        Order = dl1.Order
                    };
                    return selectItemModel;
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }
        public static Item Insert(Item model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Items.FirstOrDefault(x => x.DeviceItemId == model.DeviceItemId);
                //if (get != null)
                //{
                //    return null;
                //}
                db.Items.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;

            }
        }

        public static Item Edit(Item model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Items.FirstOrDefault(x => x.Id == model.Id);
                var getDeviceItemId = db.Items.FirstOrDefault(x => x.DeviceItemId == model.DeviceItemId);
                if (get == null)
                {
                    return null;
                }
                get.Banner = model.Banner;
                get.Description = model.Description;
                get.DeviceId = model.DeviceId;
                get.DeviceItemId = model.DeviceItemId;
                get.Name = model.Name;
                get.Note = model.Note;
                get.GroupItemId = model.GroupItemId;
                get.FriendlyName = model.FriendlyName;
                get.GroupComposedId = model.GroupComposedId;
                get.GroupSingerId = model.GroupSingerId;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Items.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.Items.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
