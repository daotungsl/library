﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.Data.Model;

namespace TN.Shared.DTO
{
    public class DevicesController
    {
        public static List<Device> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.Devices.ToList();
            }
        }

        public static Device GetById(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Devices.FirstOrDefault(x => x.Id == id);
            }
        }
        public static Device GetByImei(string imei)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Devices.FirstOrDefault(x => x.IMEI == imei);
            }
        }
        public static Device GetByName(string name)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Devices.FirstOrDefault(x => x.Name == name);
            }
        }
        public static Device GetByPort(int? port)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Devices.FirstOrDefault(x => x.PORT == port);
            }
        }
        public static List<DeviceModel> GetAllWithTray()
        {

            using (var db = new LibraryDataContext())
            {
                var dl1 = db.DeviceItems.GroupBy(x => x.DeviceId).Select(x => new { DeviceId = x.Key, Count = x.Count() });
                var list = GetAll().Select(item => new DeviceModel
                {
                    Id = item.Id,
                    IMEI = item.IMEI,
                    IP = item.IP,
                    Name = item.Name,
                    Address = item.Address,
                    PORT = item.PORT,
                    Tray = dl1.FirstOrDefault(x => x.DeviceId == item.Id) == null ? 0 : dl1.FirstOrDefault(x => x.DeviceId == item.Id).Count,
                    COM = item.COM,
                    Status = item.Status
                });
                return list.ToList();
            }
        }
        public static Tuple<List<DeviceModel>, int> FullTextFind(
            int skip, int next,
            string key
            )
        {
            using (var db = new LibraryDataContext())
            {
                var dl = db.DeviceItems.GroupBy(x => x.DeviceId).Select(x => new { DeviceId = x.Key, Count = x.Count() });

                var qr = db.Devices.AsQueryable();
                if (key != null)
                {
                    qr = db.Devices.Where(x =>
                 (x.IMEI.Contains(key) ||
                 x.IP.Contains(key) || 
                 x.Name.Contains(key) || 
                 x.Address.Contains(key) || 
                 x.Description.Contains(key) || 
                 x.COM.Contains(key) ||
                 key == null)
                ).AsQueryable();
                }
                var qr1 = qr.Skip(skip).Take(next).Select(item => new DeviceModel
                {

                    Id = item.Id,
                    IMEI = item.IMEI,
                    Description = item.Description,
                    Name = item.Name,
                    COM = item.COM,
                    Address = item.Address,
                    IP = item.IP,
                    PORT = item.PORT,
                    Tray = dl.FirstOrDefault(x => x.DeviceId == item.Id) == null ? 0 : dl.FirstOrDefault(x => x.DeviceId == item.Id).Count,
                    Status = item.Status
                }).AsQueryable();
                try
                {

                    var list = qr1.ToList();
                    return Tuple.Create(list, qr.Count());
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }

                return null;

            }
        }

        public static Device Insert(Device model)
        {
            using (var db = new LibraryDataContext())
            {
                //var checkImei = GetByImei(model.IMEI);
                //var checkPort = GetByPort(model.PORT);
                //if (checkImei != null)
                //{
                //    return checkImei;
                //}
                //if (checkPort != null)
                //{
                //    return checkPort;
                //}
                model.IMEI = "001";
                model.PORT = 80;
                model.Status = 1;

                db.Devices.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static Device Edit(Device model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Devices.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.Name = model.Name;
                get.Address = model.Address;
                get.Description = model.Description;
                get.IMEI = model.IMEI;
                get.IP = model.IP;
                get.PORT = model.PORT;
                get.COM = model.COM;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Devices.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    var listDeviceItems = DeviceItemsController.GetByDeviceId(id);
                    //var listDeviceItems = db.DeviceItems.Where(DI => DI.DeviceId.Equals(get.Id)).ToList();

                    foreach (var deviceItem in listDeviceItems)
                    {
                        var listItem = db.Items
                            .Where(i => i.DeviceId.Equals(get.Id) && i.DeviceItemId.Equals(deviceItem.Id)).ToList();
                        foreach (var item in listItem)
                        {
                            ItemsController.Delete(item.Id);
                            //db.Items.DeleteOnSubmit(item);
                        }
                        DeviceItemsController.Delete(deviceItem.Id);
                        //db.DeviceItems.DeleteOnSubmit(deviceItem);
                    }
                    db.Devices.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
