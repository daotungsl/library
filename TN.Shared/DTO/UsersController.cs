﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;

namespace TN.Shared.DTO
{
    public class UsersController
    {
        public static List<User> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.Users.Where(x => x.Username != "superadmin").ToList();
            }
        }

        public static User Login(string username, string password)
        {
            
                using (var db = new LibraryDataContext())
                {
                    var kt = db.Users.FirstOrDefault(x => x.Username == username && x.Password == password);
                    if (kt == null)
                    {
                        return null;
                    }
                    return kt;
                }
                      
          
        }

        public static User GetById(int id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Users.FirstOrDefault(x => x.Id == id);
            }
        }

        public static User Insert(User model)
        {
            using (var db = new LibraryDataContext())
            {
                db.Users.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static User Edit(User model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Users.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.DisplayName = model.DisplayName;
                get.Lock = model.Lock;
                get.Password = model.Password;
                get.Username = model.Username;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.Users.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.Users.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
