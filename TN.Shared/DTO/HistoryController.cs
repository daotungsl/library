﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;

namespace TN.Shared.DTO
{
   public class HistoryController
    {
        public static List<History> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.Histories.ToList();
            }
        }



        public static History GetById(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.Histories.FirstOrDefault(x => x.Id == id);
            }
        }

        public static History Insert(History model)
        {
            using (var db = new LibraryDataContext())
            {
                model.CreatedAt = DateTime.Now;
                db.Histories.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static GroupSinger Edit(GroupSinger model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupSingers.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.Name = model.Name;
                get.FriendlyName = model.FriendlyName;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupSingers.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.GroupSingers.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
