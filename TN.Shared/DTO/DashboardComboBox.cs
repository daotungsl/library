﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Shared.DTO
{
    public class DashboardComboBox : IEquatable<DashboardComboBox> , IComparable<DashboardComboBox>
    {
        
        public string Name { get; set; }

        public int Id { get; set; }

        public override string ToString()
        {
            return "ID: " + Id + "   Name: " + Name;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            DashboardComboBox objAsDashboardComboBox = obj as DashboardComboBox;
            if (objAsDashboardComboBox == null) return false;
            else return Equals(objAsDashboardComboBox);
        }
        public int SortByNameAscending(string name1, string name2)
        {

            return name1.CompareTo(name2);
        }

        // Default comparer for DashboardComboBox type.
        public int CompareTo(DashboardComboBox compareDashboardComboBox)
        {
            // A null value means that this object is greater.
            if (compareDashboardComboBox == null)
                return 1;

            else
                return this.Id.CompareTo(compareDashboardComboBox.Id);
        }
        public override int GetHashCode()
        {
            return Id;
        }
        public bool Equals(DashboardComboBox other)
        {
            if (other == null) return false;
            return (this.Id.Equals(other.Id));
        }
    }
}
