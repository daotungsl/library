﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;

namespace TN.Shared.DTO
{
   public class GroupSingerController
    {

        public static List<GroupSinger> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupSingers.ToList();
            }
        }

       

        public static GroupSinger GetById(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupSingers.FirstOrDefault(x => x.Id == id);
            }
        }

        public static GroupSinger Insert(GroupSinger model)
        {
            using (var db = new LibraryDataContext())
            {
                model.CreatedDate = DateTime.Now;
                db.GroupSingers.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static GroupSinger Edit(GroupSinger model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupSingers.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.Name = model.Name;
                get.FriendlyName = model.FriendlyName;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupSingers.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.GroupSingers.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
