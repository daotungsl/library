﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;
using TN.Shared.Data.Model;

namespace TN.Shared.DTO
{
    public class DeviceItemsController
    {
        public static List<DeviceItem> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.DeviceItems.ToList();
            }
        }
        public static List<SelectItemModel> GetAllById(int? id)
        {
            var newItems = new List<SelectItemModel>();

            using (var db = new LibraryDataContext())
            {
                try
                {
                    var dl = db.DeviceItems.Where(x=> x.DeviceId== id).ToList();
                    var items = db.Items.Where(x => x.DeviceId == id).Select(x=> new { DeviceId = x.DeviceId, Id = x.Id, x.DeviceItemId }).ToList();
                    foreach(var item in dl)
                    {
                        if(items.Any(x=>x.DeviceItemId==item.Id))
                        {
                            newItems.Add(new SelectItemModel { Id = item.Id, Name = $"{item.Order} (Đang sử dụng)" });
                        }
                        else
                        {
                            newItems.Add(new SelectItemModel { Id = item.Id, Name = item.Order.ToString() });
                        }
                    }
                } 
                catch(Exception ex)
                {
                   
                }
            }
            return newItems;
        }

        public static DeviceItem GetById(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.DeviceItems.FirstOrDefault(x => x.Id == id);
            }
        }
        
        public static DeviceItem GetByOrder(int? order, int? deviceId)
        {
            using (var db = new LibraryDataContext())
            {
                return db.DeviceItems.FirstOrDefault(x => x.Order == order && x.DeviceId == deviceId);
            }
        }
        public static List<DeviceItem> GetByDeviceId(int deviceId)
        {
            using (var db = new LibraryDataContext())
            {
                return db.DeviceItems.Where(x => x.DeviceId == deviceId).ToList();
            }
        }

        public static DeviceItem Insert(DeviceItem model)
        {
            using (var db = new LibraryDataContext())
            {
                var checkOrder = GetByOrder(model.Order, model.DeviceId);
                if (checkOrder != null) return null;
                db.DeviceItems.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;

            }
        }

        public static DeviceItem Edit(DeviceItem model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.DeviceItems.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.Order = model.Order;
                get.Status = model.Status;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.DeviceItems.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.DeviceItems.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }

        
    }
}
