﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;

namespace TN.Shared.DTO
{
   public class GroupComposedController
    {
        public static List<GroupComposed> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupComposeds.ToList();
            }
        }



        public static GroupComposed GetById(int? id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupComposeds.FirstOrDefault(x => x.Id == id);
            }
        }

        public static GroupComposed Insert(GroupComposed model)
        {
            using (var db = new LibraryDataContext())
            {
                model.CreatedDate = DateTime.Now;
                db.GroupComposeds.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static GroupComposed Edit(GroupComposed model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupComposeds.FirstOrDefault(x => x.Id == model.Id);
                if (get == null)
                {
                    return null;
                }
                get.Name = model.Name;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupComposeds.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.GroupComposeds.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }
    }
}
