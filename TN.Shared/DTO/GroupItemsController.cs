﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Shared.Data;

namespace TN.Shared.DTO
{
    public  class GroupItemsController
    {
        public static List<GroupItem> GetAll()
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupItems.ToList();
            }
        }
        public static GroupItem GetByName(string name)
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupItems.FirstOrDefault(x => x.Name == name);
            }
        }
        public static GroupItem GetById(int id)
        {
            using (var db = new LibraryDataContext())
            {
                return db.GroupItems.FirstOrDefault(x => x.Id == id);
            }
        }

        public static GroupItem Insert(GroupItem model)
        {
            using (var db = new LibraryDataContext())
            {
                model.CreatedDate = DateTime.Now;
                db.GroupItems.InsertOnSubmit(model);
                db.SubmitChanges();
                return model;
            }
        }

        public static GroupItem Edit(GroupItem model)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupItems.FirstOrDefault(x => x.Id == model.Id);
                if(get==null)
                {
                    return null;
                }
                get.Name = model.Name;
                db.SubmitChanges();
                return model;
            }
        }

        public static void Delete(int id)
        {
            using (var db = new LibraryDataContext())
            {
                var get = db.GroupItems.FirstOrDefault(x => x.Id == id);
                if (get != null)
                {
                    db.GroupItems.DeleteOnSubmit(get);
                    db.SubmitChanges();
                }
            }
        }

        
    }
}
